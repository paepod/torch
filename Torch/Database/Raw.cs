﻿namespace Torch.Database
{
    public class Raw
    {
        public object Value { get; set; }

        public Raw(object xValue)
        {
            Value = xValue;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
