﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Torch.Database.Models;

namespace Torch.Database.Query
{
    public class SQLiteSyntax : BasicSqlSyntax
    {

        public override string CompileBasicJoin(SearchCondition sc)
        {
            if (sc.JoinType == "RIGHT")
            {
                sc.JoinType = "LEFT";
                var temp = sc.Column1;
                sc.Column1 = sc.Column2;
                sc.Column2 = temp;
            }
            return string.Format("{0} JOIN {1} ON ({2} {3} {4}) ", sc.JoinType, sc.Table, sc.Column1, sc.Operator, sc.Column2);
        }


        public override string CompileInsert(Builder xBuilder, object[] xValues)
        {
            // We use normal sql to insert single record but
            // for insert mutiple recordd we need to convert sql below 
            // INSERT INTO 'tablename'('column1', 'column2') VALUES ('data1', 'data2'),('data1', 'data2'),
            // INSERT INTO 'tablename' ('column1', 'column2') SELECT 'data1' AS 'column1', 'data2' AS 'column2' 
            // UNION ALL SELECT 'data1' AS 'column1', 'data2' AS 'column2'

            if (xValues == null || xValues.Length == 0)
                return null;

            if (xValues.Length == 1)
                return base.CompileInsert(xBuilder, xValues);

            PropertyInfo[] properties = xValues[0].GetType().GetProperties();
            var columnsList = new List<object>();
            for (int i = 0; i < properties.Length; i++)
                columnsList.Add(properties[i].Name);

            string columns = CompileColumns(columnsList);

            var valueList = new List<string>();
            var parameterList = new List<string>();

            var valueSet = new StringBuilder();
            for (int i = 0; i < xValues.Length; i++)
            {
                PropertyInfo[] property = xValues[i].GetType().GetProperties();
                for (int j = 0; j < property.Length; j++)
                {
                    object paramValue = property[j].GetValue(xValues[i], null);
                    string paramName = xBuilder.GetAndAddParamName(paramValue);

                    valueSet.AppendFormat("{0} AS '{1}', ", paramName, property[j].Name);
                }
                valueSet.Remove(valueSet.Length - 2, 2);
                valueSet.Append("UNION ALL SELECT");
            }
            valueSet.Remove(valueSet.Length - 16, "UNION ALL SELECT".Length);
            var sql = String.Format("INSERT INTO {0} ({1}) SELECT {2};", xBuilder.Table, columns, valueSet.ToString());
            return sql;
        }

        public override string CompileTruncate(Builder xBuilder)
        {
            return String.Format("DELETE FROM {0}; DELETE FROM SQLITE_SEQUENCE WHERE name = '{1}';", 
                xBuilder.Table, xBuilder.Table);
        }
    }
}
