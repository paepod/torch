﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Torch.Database.Models;

namespace Torch.Database.Query
{
    public class BasicSqlSyntax : IBasicSqlSyntax
    {
        public virtual string CompileSelect(Builder xQueryBuilder)
        {
            var query = new StringBuilder();
            query.Append("SELECT ");
            query.Append(CompileColumns(xQueryBuilder.Columns));
            query.Append(CompileFrom(xQueryBuilder.Table));
            query.Append(CompileJoin(xQueryBuilder.JoinClauses));
            query.Append(CompileWhere(xQueryBuilder.WhereClauses));
            query.Append(CompileUnion(xQueryBuilder.UnionClauses));
            query.Append(CompileGroupBy(xQueryBuilder.Groups));
            query.Append(CompileHaving(xQueryBuilder.HavingClauses));
            query.Append(CompileOrderBy(xQueryBuilder.Orders));
            query.Append(CompileSkipAndTake(xQueryBuilder.Offset, xQueryBuilder.Fetch));
            return query.ToString().Trim();
        }

        public virtual string CompileColumns(List<object> xColumns)
        {
            if (xColumns.Count == 0)
                xColumns.Add("*");

            for (int i = 0; i < xColumns.Count; i++)
            {
                if (xColumns[i] is SearchCondition)
                {
                    var sc = (SearchCondition)xColumns[i];
                    string query = "(" + CompileSubQuery(sc.Query) + ") AS " + sc.Column1;
                    xColumns[i] = query;
                }
            }
            return string.Join(", ", xColumns);
        }

        public virtual string CompileFrom(object table)
        {
            if (table is SearchCondition)
            {
                var sc = (SearchCondition)table;

                if (String.IsNullOrWhiteSpace(sc.Column1) == false)
                {
                    sc.Column1 = " AS " + sc.Column1.Trim();
                }
                return string.Format(" FROM ({0}){1}", CompileSubQuery(sc.Query), sc.Column1);
            }

            if (table is string)
                return string.Format(" FROM {0} ", table);

            return "";

        }

        public virtual string CompileJoin(List<SearchCondition> xJoinClauses)
        {
            if (xJoinClauses == null || xJoinClauses.Count == 0)
                return "";

            return CompileCondition(xJoinClauses);

        }

        public virtual string CompileWhere(List<SearchCondition> xWhereClauses)
        {
            if (xWhereClauses == null || xWhereClauses.Count == 0)
                return "";

            return "WHERE" + CompileCondition(xWhereClauses);

        }

        public virtual string CompileSubQuery(Builder xQueryBuilder)
        {
            return CompileSelect(xQueryBuilder);
        }

        public virtual string CompileCondition(List<SearchCondition> xConditions)
        {
            var sb = new StringBuilder();
            string subQuery;
            foreach (SearchCondition sc in xConditions)
            {
                switch (sc.Type)
                {
                    case SearchCondition.Types.Basic:
                        sb.Append(CompileSearchConditionBasic(sc));
                        break;
                    case SearchCondition.Types.BasicJoin:
                        sb.Append(CompileBasicJoin(sc));
                        break;
                    case SearchCondition.Types.ComplexJoin:
                        sb.Append(CompileComplexJoin(sc));
                        break;
                    case SearchCondition.Types.Between:
                        sb.Append(CompileSearchConditionBetween(sc));
                        break;
                    case SearchCondition.Types.In:
                        sb.Append(CompileSearchConditionIn(sc));
                        break;
                    case SearchCondition.Types.InSub:
                        sb.Append(CompileSearchConditionInSub(sc));
                        break;
                    case SearchCondition.Types.Exits:
                        sb.Append(CompileSearchConditionExits(sc));
                        break;
                    case SearchCondition.Types.Nasted:
                        sb.Append(string.Join(" ", sc.Boolean, "(" + CompileSearchConditionBasic(sc.Query.WhereClauses).Trim() + ") "));
                        break;
                    case SearchCondition.Types.SubQuery:
                        subQuery = "(" + CompileSubQuery(sc.Query) + ")";
                        sb.Append(string.Join(" ", sc.Boolean, "(" + string.Join(" ", sc.Column1, sc.Operator, subQuery) + ") "));
                        break;
                    case SearchCondition.Types.Union:
                        sb.Append(string.Join(" ", CompileUnion(sc)));
                        break;
                }
            }
            return sb.ToString();
        }

        public virtual string CompileBasicJoin(SearchCondition sc)
        {
            return string.Format("{0} JOIN {1} ON ({2} {3} {4}) ", sc.JoinType, sc.Table, sc.Column1, sc.Operator, sc.Column2);
        }

        public virtual string CompileComplexJoin(SearchCondition sc)
        {
            return CompileBasicJoin(sc) + string.Join(" ", sc.Boolean, "(" + CompileCondition(sc.Query.WhereClauses).Trim() + ") ");
        }

        public virtual string CompileSearchConditionBasic(List<SearchCondition> xSearchConditions)
        {
            var sb = new StringBuilder();
            foreach (SearchCondition sc in xSearchConditions)
            {
                sb.Append(CompileSearchConditionBasic(sc));
            }
            return sb.ToString().Trim();
        }

        public virtual string CompileSearchConditionBasic(SearchCondition sc)
        {
            if (!(string.IsNullOrEmpty(sc.Column1) && string.IsNullOrEmpty(sc.Operator) && string.IsNullOrEmpty(sc.ParamName1)))
                return string.Format("{0} {1} {2} {3} ", sc.Boolean, sc.Column1, sc.Operator, sc.ParamName1);

            return string.Join(" ", string.Format(" {0} ", sc.Boolean));
        }

        public virtual string CompileSearchConditionBetween(SearchCondition sc)
        {
            var between = (sc.Not == false) ? "{0} {1} BETWEEN {2} AND {3} " : " {0} {1} NOT BETWEEN {2} AND {3} ";
            return string.Join(" ", string.Format(between, sc.Boolean, sc.Column1, sc.ParamName1, sc.ParamName2));
        }

        public virtual string CompileSearchConditionIn(SearchCondition sc)
        {
            var ins = (sc.Not == false) ? " IN " : " NOT IN ";
            return string.Join(" ", sc.Boolean, sc.Column1 + ins + "(" + sc.ParamName1 + ") ");
        }

        public virtual string CompileSearchConditionInSub(SearchCondition sc)
        {
            var ins = (sc.Not == false) ? " IN " : " NOT IN ";
            string select = CompileSubQuery(sc.Query);
            return string.Join(" ", sc.Boolean, sc.Column1 + ins + "(" + select + ") ");
        }

        public virtual string CompileSearchConditionExits(SearchCondition sc)
        {
            var exits = (sc.Not == false) ? "EXITS " : "NOT EXITS ";
            string select = CompileSubQuery(sc.Query);
            return string.Join(" ", sc.Boolean, exits + "(" + select + ") ");
        }

        public virtual string CompileUnion(SearchCondition sc)
        {
            string union = (sc.Not == false) ? "UNION" : "UNION ALL";
            return string.Join(" ", union, CompileSelect(sc.Query) + " ");
        }

        public virtual string CompileUnion(List<SearchCondition> xUnionClauses)
        {
            if (xUnionClauses == null || xUnionClauses.Count == 0)
                return "";

            return CompileCondition(xUnionClauses);
        }

        public virtual string CompileGroupBy(List<string> xGroup)
        {
            if (xGroup == null || xGroup.Count == 0)
                return "";

            return "GROUP BY " + string.Join(", ", xGroup) + " ";
        }

        public virtual string CompileHaving(List<SearchCondition> xHavingClauses)
        {
            if (xHavingClauses == null || xHavingClauses.Count == 0)
                return "";

            return "HAVING" + CompileCondition(xHavingClauses);
        }

        public virtual string CompileOrderBy(List<string> xOrder)
        {
            if (xOrder == null || xOrder.Count == 0)
                return "";

            return "ORDER BY " + string.Join(", ", xOrder) + " ";
        }

        public virtual string CompileSkipAndTake(int? skip, int? take)
        {
            string _skip = "", _take = "";

            if (skip != null)
            {
                _skip = "OFFSET " + skip.ToString();
            }
            if (take != null)
            {
                _take = "LIMIT " + take.ToString();
            }
            if (_take == "" && _skip != "")
            {
                _take = "LIMIT 0";
            }

            return String.Format("{0} {1}", _take, _skip).Trim();
        }

        public virtual void CompileStatement(Builder xBuilder, object[] xValues)
        {
            if (xValues.Length > 0)
            {
                for (int i = 0; i < xValues.Length; i++)
                {
                    PropertyInfo[] property = xValues[i].GetType().GetProperties();
                    for (int j = 0; j < property.Length; j++)
                    {
                        object paramValue = property[j].GetValue(xValues[i], null);
                        string paramName = property[j].Name.StartsWith("@", StringComparison.Ordinal) ? property[j].Name : "@" + property[j].Name ;
                        xBuilder.SetParamName("@" + property[j].Name, paramValue);
                    }
                }
            }
        }


        public virtual string CompileInsert(Builder xBuilder, object[] xValues)
        {
            PropertyInfo[] properties = xValues[0].GetType().GetProperties();
            var columnsList = new List<object>();
            for (int i = 0; i < properties.Length; i++)
            {
                columnsList.Add(properties[i].Name);
            }
            string columns = CompileColumns(columnsList);

            var valueSet = new StringBuilder();
            for (int i = 0; i < xValues.Length; i++)
            {
                PropertyInfo[] property = xValues[i].GetType().GetProperties();
                valueSet.Append(" (");
                for (int j = 0; j < property.Length; j++)
                {
                    object paramValue = property[j].GetValue(xValues[i], null);
                    string paramName = xBuilder.GetAndAddParamName(paramValue);
                    valueSet.Append(paramName + ",");
                }
                valueSet.Remove(valueSet.Length - 1, 1);
                valueSet.Append(" ),");
            }
            valueSet.Remove(valueSet.Length - 1, 1);
            return String.Format("INSERT INTO {0} ({1}) VALUES{2};", xBuilder.Table, columns, valueSet.ToString());
        }

        public virtual string CompileUpdate(Builder xBuilder, object xValues)
        {
            var columns = new StringBuilder();

            PropertyInfo[] property = xValues.GetType().GetProperties();
            for (int i = 0; i < property.Length; i++)
            {
                object paramValue = property[i].GetValue(xValues, null);
                string paramName = xBuilder.GetAndAddParamName(paramValue);
                columns.AppendFormat("{0} = {1},", property[i].Name, paramName);
            }
            columns.Remove(columns.Length - 1, 1);

            var where = CompileWhere(xBuilder.WhereClauses);

            return String.Format("UPDATE {0} SET {1} {2}", xBuilder.Table, columns.ToString(), where).Trim() + ";";
        }

        public virtual string CompileDelete(Builder xBuilder)
        {
            if (xBuilder.WhereClauses.Count > 0)
                return string.Format("DELETE from {0} {1}", xBuilder.Table, CompileWhere(xBuilder.WhereClauses)).Trim();

            return string.Format("DELETE from {0}", xBuilder.Table).Trim();
        }

        public virtual string CompileTruncate(Builder xBuilder)
        {
            return String.Format("TRUNCATE TABLE {0}", xBuilder.Table);
        }

        public virtual string WrapOperator(string xOperators)
        {
            if (xOperators == null || String.IsNullOrWhiteSpace(xOperators))
                xOperators = "=";
            return xOperators.Trim().ToUpper();
        }

        public virtual string WrapTable(string xValue, string xPrefix = null)
        {
            xValue = WrapValue(xValue);
            xPrefix = String.IsNullOrWhiteSpace(xPrefix) ? "" : WrapValue(xPrefix) + ".";
            return xPrefix + xValue;
        }

        public virtual string WrapValue(string xValue)
        {
            return Wrap(xValue, "\"", "\"\"", "\"");
        }

        protected virtual string Wrap(string value, string prefix, string replace, string subfix)
        {
            value = Regex.Replace(value, @"\s+", " ").Trim();
            string[] values;

            if (value.ToLower().Contains(" as "))
            {
                values = value.Split(' ');

                values[0] = WrapValue(values[0]);
                values[2] = WrapValue(values[2]);
                return values[0] + " AS " + values[2];
            }
            if (value.ToLower().Contains(" "))
            {
                values = value.Split(' ');
                var segments = values[0].Split('.');
                var sb = new StringBuilder();
                foreach (var segment in segments)
                {
                    sb.Append(WrapValue(segment) + ".");
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append(" AS ");

                sb.Append(WrapValue(values[1]));
                return sb.ToString();
            }
            return prefix + value.Replace(subfix, replace) + subfix;
        }
    }

}
