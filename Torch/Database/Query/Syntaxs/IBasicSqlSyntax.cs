﻿using System.Collections.Generic;
using Torch.Database.Models;

namespace Torch.Database.Query
{
    public interface IBasicSqlSyntax
    {
        string CompileBasicJoin(SearchCondition sc);
        string CompileColumns(List<object> xColumns);
        string CompileComplexJoin(SearchCondition sc);
        string CompileCondition(List<SearchCondition> xConditions);
        string CompileDelete(Builder xBuilder);
        string CompileFrom(object table);
        string CompileGroupBy(List<string> xGroup);
        string CompileHaving(List<SearchCondition> xHavingClauses);
        string CompileInsert(Builder xBuilder, object[] xValues);
        string CompileJoin(List<SearchCondition> xJoinClauses);
        string CompileOrderBy(List<string> xOrder);
        string CompileSearchConditionBasic(SearchCondition sc);
        string CompileSearchConditionBasic(List<SearchCondition> xSearchConditions);
        string CompileSearchConditionBetween(SearchCondition sc);
        string CompileSearchConditionExits(SearchCondition sc);
        string CompileSearchConditionIn(SearchCondition sc);
        string CompileSearchConditionInSub(SearchCondition sc);
        string CompileSelect(Builder xQueryBuilder);
        string CompileSkipAndTake(int? skip, int? take);
        void CompileStatement(Builder xBuilder, object[] xValues);
        string CompileSubQuery(Builder xQueryBuilder);
        string CompileTruncate(Builder xBuilder);
        string CompileUnion(SearchCondition sc);
        string CompileUnion(List<SearchCondition> xUnionClauses);
        string CompileUpdate(Builder xBuilder, object xValues);
        string CompileWhere(List<SearchCondition> xWhereClauses);
        string WrapOperator(string xOperators);
        string WrapTable(string xValue, string xPrefix = null);
        string WrapValue(string xValue);
    }
}