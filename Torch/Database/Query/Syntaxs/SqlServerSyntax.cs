﻿using System;
using System.Text;

namespace Torch.Database.Query
{
    public class SqlServerSyntax : BasicSqlSyntax
    {
        public override string CompileSelect(Builder xQueryBuilder)
        {
            var b = xQueryBuilder;
            var query = new StringBuilder();
            query.Append("SELECT ");

            //Compile Take Only
            if (b.Fetch > 0 && (b.Offset == null || b.Offset <= 0))
                query.Append("TOP " + b.Fetch + " ");

            query.Append(CompileColumns(b.Columns));

            //Compile Take and Skip
            if (b.Offset != null && b.Offset > 0)
            {
                return CompileFetchOffset(b, query);
            } 

            CompileTebleExpression(b, query);
            query.Append(CompileOrderBy(b.Orders));
            return query.ToString().Trim();

        }

        protected string CompileFetchOffset(Builder b, StringBuilder query)
        {
            CompileOverOrder(b, query);
            CompileTebleExpression(b, query);
            return CompileConstraint(b, query);
        }

        protected void CompileTebleExpression(Builder b, StringBuilder query)
        {
            query.Append(CompileFrom(b.Table));
            query.Append(CompileJoin(b.JoinClauses));
            query.Append(CompileWhere(b.WhereClauses));
            query.Append(CompileUnion(b.UnionClauses));
            query.Append(CompileGroupBy(b.Groups));
            query.Append(CompileHaving(b.HavingClauses));
        }

        public override string CompileSkipAndTake(int? skip, int? take)
        {
            return "";
        }

        protected string CompileConstraint(Builder b, StringBuilder query)
        {
            var p1 = b.Offset + 1;

            string constraint = null;
            if (b.Fetch > 0)
            {
                var p2 = b.Offset + b.Fetch;
                constraint = String.Format("BETWEEN {0} AND {1}", p1, p2);
            }
            else
            {
                constraint = String.Format(">= {0}", p1);
            }

            return String.Format("SELECT * FROM ({0}) AS TEMP_TABLE WHERE ROW_NUM {1}", query.ToString().Trim(), constraint);
        }

        protected void CompileOverOrder(Builder b, StringBuilder query)
        {
            string orderBy;
            if (b.Orders.Count == 0)
            {
                orderBy = ", ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS ROW_NUM";
            }
            else
            {
                orderBy = String.Format(", ROW_NUMBER() OVER (ORDER BY {0}) AS ROW_NUM", CompileOrderBy(b.Orders).Replace("ORDER BY ", "").Trim());
            }
            query.Append(orderBy.Trim());

        }

        public override string WrapValue(string xValue)
        {
            return Wrap(xValue, "[", "]]", "]");
        }



    }
}
