﻿using System;

namespace Torch.Database.Query
{
    public class PostgresSyntax : BasicSqlSyntax
    {
        public override string CompileTruncate(Builder xBuilder)
        {
            return String.Format("TRUNCATE TABLE {0} RESTART IDENTITY;", xBuilder.Table).Trim();
        }
    }
}
