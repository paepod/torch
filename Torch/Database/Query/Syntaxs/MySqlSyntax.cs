﻿using System.Text;
using Torch.Database.Models;

namespace Torch.Database.Query
{
    public class MySqlSyntax : BasicSqlSyntax
    {
        public override string WrapValue(string xValue)
        {
            return Wrap(xValue, "`", "``", "`");
        }

    }
}
