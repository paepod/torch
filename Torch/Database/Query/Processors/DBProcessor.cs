﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Torch.Config;
using Dapper;

namespace Torch.Database.Query.Processors
{

    public static class DBProcessor
    {
        public static IEnumerable<dynamic> ExecuteQuery(string commandText, DbConfig dbConfig, Dictionary<string, object> parameters)
        {

            DbProviderFactory dbf = DbProviderFactories.GetFactory(dbConfig.DbProvider.Name);
            using (DbConnection dbConnection = dbf.CreateConnection())
            {
                OpenConnection(dbConfig, dbConnection);
                return dbConnection.Query(commandText, parameters);
            }
        }

        public static IEnumerable<T> ExecuteQuery<T>(string commandText, DbConfig dbConfig, Dictionary<string, object> parameters)
        {

            DbProviderFactory dbf = DbProviderFactories.GetFactory(dbConfig.DbProvider.Name);
            using (DbConnection dbConnection = dbf.CreateConnection())
            {
                OpenConnection(dbConfig, dbConnection);
                return dbConnection.Query<T>(commandText, parameters);
            }
        }



        public static int ExecuteNonQuery(string commandText, DbConfig dbConfig, params DbParameter[] parameters)
        {
            int result = 0;
            DbProviderFactory dbf = DbProviderFactories.GetFactory(dbConfig.DbProvider.Name);

            using (DbConnection dbConnection = dbf.CreateConnection())
            {
                OpenConnection(dbConfig, dbConnection);

                using (DbCommand dbCommand = dbConnection.CreateCommand())
                {
                    SetCommand(commandText, dbConnection, dbCommand);
                    foreach (var p in parameters)
                        dbCommand.Parameters.Add(p);

                    result = dbCommand.ExecuteNonQuery();
                    dbCommand.Parameters.Clear();
                    dbConnection.Close();
                }

            }
            return result;
        }

        public static int ExecuteNonQuery(string commandText, DbConfig dbConfig, Dictionary<string, object> parameters)
        {
            int result = 0;
            DbProviderFactory dbf = DbProviderFactories.GetFactory(dbConfig.DbProvider.Name);
            using (DbConnection dbConnection = dbf.CreateConnection())
            {
                OpenConnection(dbConfig, dbConnection);

                using (DbCommand dbCommand = dbConnection.CreateCommand())
                {
                    SetCommand(commandText, dbConnection, dbCommand);
                    AddParameter(dbCommand, parameters);
                    result = dbCommand.ExecuteNonQuery();
                    dbCommand.Parameters.Clear();
                    dbConnection.Close();
                }

            }
            return result;
        }

        public static object ExecuteScalar(string commandText, DbConfig dbConfig, params DbParameter[] parameters)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory(dbConfig.DbProvider.Name);
            object result;
            using (DbConnection dbConnection = dbf.CreateConnection())
            {
                OpenConnection(dbConfig, dbConnection);

                using (DbCommand dbCommand = dbConnection.CreateCommand())
                {
                    SetCommand(commandText, dbConnection, dbCommand);

                    foreach (var p in parameters)
                    {
                        dbCommand.Parameters.Add(p);
                    }

                    result = dbCommand.ExecuteScalar();
                }
            }

            return null;
        }

        public static object ExecuteScalar(string commandText, DbConfig dbConfig, Dictionary<string, object> parameters)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory(dbConfig.DbProvider.Name);
            object result;
            using (DbConnection dbConnection = dbf.CreateConnection())
            {
                OpenConnection(dbConfig, dbConnection);

                using (DbCommand dbCommand = dbConnection.CreateCommand())
                {
                    SetCommand(commandText, dbConnection, dbCommand);

                    AddParameter(dbCommand, parameters);

                    result = dbCommand.ExecuteScalar();
                }
            }

            return result;

        }

        public static DataSet ExecuteDataset(string commandText, DbConfig dbConfig, params DbParameter[] parameters)
        {

            DbProviderFactory dbf = DbProviderFactories.GetFactory(dbConfig.DbProvider.Name);
            using (DbConnection dbConnection = dbf.CreateConnection())
            {
                OpenConnection(dbConfig, dbConnection);

                using (DbCommand dbCommand = dbConnection.CreateCommand())
                {
                    SetCommand(commandText, dbConnection, dbCommand);

                    foreach (var p in parameters)
                    {
                        dbCommand.Parameters.Add(p);
                    }

                    DataSet ds = Fill(dbf, dbConnection, dbCommand);
                    return ds;
                }
            }
        }

        public static DataSet ExecuteDataset(string commandText, DbConfig dbConfig, Dictionary<string, object> parameters)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory(dbConfig.DbProvider.Name);
            using (DbConnection dbConnection = dbf.CreateConnection())
            {
                OpenConnection(dbConfig, dbConnection);

                using (DbCommand dbCommand = dbConnection.CreateCommand())
                {
                    SetCommand(commandText, dbConnection, dbCommand);
                    AddParameter(dbCommand, parameters);

                    DataSet ds = Fill(dbf, dbConnection, dbCommand);
                    return ds;
                }
            }
        }

        private static void OpenConnection(DbConfig dbConfig, DbConnection dbcn)
        {
            dbcn.ConnectionString = dbConfig.ConnectionString;
            dbcn.Open();
        }

        private static void SetCommand(string commandText, DbConnection dbcn, DbCommand dbcmd)
        {
            dbcmd.Connection = dbcn;
            dbcmd.CommandType = CommandType.Text;
            dbcmd.CommandText = commandText;
        }

        private static DataSet Fill(DbProviderFactory dbf, DbConnection dbcn, DbCommand dbcmd)
        {
            var da = dbf.CreateDataAdapter();
            da.SelectCommand = dbcmd;
            var ds = new DataSet();
            da.Fill(ds);
            dbcmd.Parameters.Clear();
            dbcn.Close();
            return ds;
        }

        private static void AddParameter(DbCommand dbCmd, Dictionary<string, object> parameters)
        {
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    var p = dbCmd.CreateParameter();
                    p.ParameterName = parameter.Key;
                    if (parameter.Value == null)
                        p.Value = DBNull.Value;

                    if (parameter.Value is Guid)
                    {
                        p.Value = parameter.Value.ToString();
                        p.DbType = DbType.String;
                        p.Size = 4000;
                    }
                    else
                    {
                        p.Value = parameter.Value;
                    }
                    dbCmd.Parameters.Add(p);
                }
            }
        }
    }
}