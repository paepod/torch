﻿using Torch.Database.Query;

namespace Torch.Database.Models
{
    public class SearchCondition
    {
        public enum Types
        {
            Basic,
            BasicJoin,
            ComplexJoin,
            Nasted,
            SubQuery,
            Between,
            In,
            InSub,
            Exits,
            Union,
        };


        public Types Type { get; set; }
        public string Table { get; set; }
        public string JoinType { get; set; }
        public string Operator { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string ParamName1 { get; set; }
        public string ParamName2 { get; set; }
        public string Boolean { get; set; }
        public bool Not { get; set; }
        public Builder Query { get; set; }



    }
}
