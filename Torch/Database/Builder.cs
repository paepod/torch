﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Torch.Config;
using Torch.Database.Models;
using Torch.Database.Query;
using Torch.Database.Query.Processors;

namespace Torch.Database
{
    public class Builder
    {
        protected object SyncLock = new object();
        protected int Counter = 0;

        public virtual object Table { get; set; }
        public virtual int? Offset { get; set; }
        public virtual int? Fetch { get; set; }

        protected List<object> _Columns;
        public virtual List<object> Columns
        {
            get
            {
                lock (SyncLock)
                {
                    if (_Columns == null)
                        _Columns = new List<object>();
                    return _Columns;
                }
            }
            set { _Columns = value; }
        }

        protected List<string> _Groups;
        public virtual List<string> Groups
        {
            get
            {
                lock (SyncLock)
                {
                    if (_Groups == null)
                        _Groups = new List<string>();
                    return _Groups;
                }
            }
            set { _Groups = value; }
        }

        protected List<string> _Orders;
        public virtual List<string> Orders
        {
            get
            {
                lock (SyncLock)
                {
                    if (_Orders == null)
                        _Orders = new List<string>();
                    return _Orders;
                }
            }
            set { _Orders = value; }
        }

        protected List<SearchCondition> _WhereClauses;
        public virtual List<SearchCondition> WhereClauses
        {
            get
            {
                lock (SyncLock)
                {
                    if (_WhereClauses == null)
                        _WhereClauses = new List<SearchCondition>();
                    return _WhereClauses;
                }
            }
            set { _WhereClauses = value; }
        }

        protected List<SearchCondition> _JoinClauses;
        public virtual List<SearchCondition> JoinClauses
        {
            get
            {
                lock (SyncLock)
                {
                    if (_JoinClauses == null)
                        _JoinClauses = new List<SearchCondition>();
                    return _JoinClauses;
                }
            }
            set { _JoinClauses = value; }
        }

        protected List<SearchCondition> _HavingClauses;
        public virtual List<SearchCondition> HavingClauses
        {
            get
            {
                lock (SyncLock)
                {
                    if (_HavingClauses == null)
                        _HavingClauses = new List<SearchCondition>();
                    return _HavingClauses;
                }
            }
            set { _HavingClauses = value; }
        }

        protected List<SearchCondition> _UnionClauses;
        public virtual List<SearchCondition> UnionClauses
        {
            get
            {
                lock (SyncLock)
                {
                    if (_UnionClauses == null)
                        _UnionClauses = new List<SearchCondition>();
                    return _UnionClauses;
                }
            }
            set { _UnionClauses = value; }
        }

        protected Dictionary<string, object> _Parameters;
        public virtual Dictionary<string, object> Parameters
        {
            get
            {
                lock (SyncLock)
                {
                    if (_Parameters == null)
                        _Parameters = new Dictionary<string, object>();
                    return _Parameters;
                }
            }
            set { _Parameters = value; }
        }

        protected BasicSqlSyntax _SqlSyntax;
        public virtual BasicSqlSyntax SqlSyntax
        {
            get
            {
                if (_SqlSyntax == null)
                    _SqlSyntax = new BasicSqlSyntax();

                return _SqlSyntax;
            }

            set
            {
                _SqlSyntax = value;

            }
        }

        protected DbConfig _DbConfig;
        public virtual DbConfig DbConfig
        {
            get
            {
                return _DbConfig;
            }

            set
            {
                _DbConfig = value;

            }
        }

        public Builder() : this(new BasicSqlSyntax())
        {
        }

        public Builder(BasicSqlSyntax xSqlSyntax, DbConfig xDbConfig = null)
        {
            _SqlSyntax = xSqlSyntax;
            _DbConfig = xDbConfig;
        }

        public Builder NewBuilder()
        {
            if (SqlSyntax is MySqlSyntax)
                return new Builder(new MySqlSyntax());

            if (SqlSyntax is SqlServerSyntax)
                return new Builder(new SqlServerSyntax());

            if (SqlSyntax is PostgresSyntax)
                return new Builder(new PostgresSyntax());

            return new Builder(new BasicSqlSyntax());
        }

        public virtual Builder Select(params string[] xQuery)
        {
            foreach (string q in xQuery)
            {
                Columns.Add(q);
            }
            return this;
        }

        public virtual Builder Select(string xAliasName, Action<Builder> closure )
        {
            Builder q = RunClosure(closure );
            var sc = new SearchCondition
            {
                Type = SearchCondition.Types.SubQuery,
                Column1 = xAliasName,
                Query = q
            };

            Columns.Add(sc);
            MergeParameter(q.Parameters);
            return this;
        }

        public virtual Builder FromSubQuery(string xAliasName, Builder builder, bool redefineParameter = true)
        {
            if (redefineParameter == true)
                RedefineParameter(builder);

            var sc = new SearchCondition
            {
                Type = SearchCondition.Types.SubQuery,
                Column1 = xAliasName,
                Query = builder
            };

            Table = sc;

            MergeParameter(builder.Parameters);

            return this;
        }

        public virtual Builder From(string table)
        {
            Table = table;
            return this;
        }

        public virtual Builder From(string xAliasName, Action<Builder> closure )
        {
            Builder q = RunClosure(closure );
            return FromSubQuery(xAliasName, q, false);
        }

        public virtual Builder From(string xAliasName, Builder builder)
        {
            return FromSubQuery(xAliasName, builder);
        }

        public virtual Builder Join(string xTable, string xColumn1, string xOperator, string xColumn2, string xJoinType = "INNER", Action<Builder> closure  = null, string xBoolean = "AND")
        {

            if (string.IsNullOrWhiteSpace(xJoinType))
            {
                xJoinType = "INNER";
            }

            var sc = new SearchCondition
            {
                Table = xTable,
                Column1 = xColumn1,
                Column2 = xColumn2,
                Operator = WrapOperator(xOperator),
                JoinType = xJoinType,
                Boolean = xBoolean
            };

            if (closure  != null)
            {
                var query = RunClosure(closure );
                sc.Type = SearchCondition.Types.ComplexJoin;
                sc.Query = query;
                MergeParameter(query.Parameters);
            }
            else
            {
                sc.Type = SearchCondition.Types.BasicJoin;
            }

            JoinClauses.Add(sc);

            return this;
        }

        public virtual Builder InnerJoin(string xTable, string xColumn1, string xColumn2, Action<Builder> closure  = null, string xOperator = "=", string xBoolean = "AND")
        {

            return Join(xTable, xColumn1, xOperator, xColumn2, closure : closure );
        }

        public virtual Builder LeftJoin(string xTable, string xColumn1, string xColumn2, Action<Builder> closure  = null, string xOperator = "=", string xBoolean = "AND")
        {
            return Join(xTable, xColumn1, xOperator, xColumn2, "LEFT", closure );
        }

        public virtual Builder RightJoin(string xTable, string xColumn1, string xColumn2, Action<Builder> closure  = null, string xOperator = "=", string xBoolean = "AND")
        {
            return Join(xTable, xColumn1, xOperator, xColumn2, "RIGHT", closure );
        }

        public virtual Builder Where(string xColumn, string xOperator, object xValue, string xBoolean = "AND")
        {
            if (WhereClauses.Count() == 0)
                xBoolean = "";

            var sc = new SearchCondition
            {
                Boolean = xBoolean,
                Column1 = xColumn,
                Operator = WrapOperator(xOperator),
                Type = SearchCondition.Types.Basic
            };

            sc.ParamName1 = GetAndAddParamName(xValue);
            WhereClauses.Add(sc);
            return this;
        }

        public virtual Builder Where(Action<Builder> closure , string booelan = "AND")
        {
            Builder q = RunClosure(closure );
            var sc = new SearchCondition
            {
                Type = SearchCondition.Types.Nasted,
                Boolean = booelan,
                Query = q
            };

            WhereClauses.Add(sc);
            MergeParameter(q.Parameters);
            return this;
        }

        public virtual Builder Where(string xColumn, object xValue)
        {
            return Where(xColumn, "=", xValue);
        }

        public virtual Builder Where(string xColumn, string xOperator, Action<Builder> closure , string xBooelan = "AND")
        {
            if (WhereClauses.Count() == 0)
                xBooelan = "";

            var sc = new SearchCondition
            {
                Column1 = xColumn,
                Operator = WrapOperator(xOperator),
                Boolean = xBooelan
            };

            // if the closure s is null,we assume that the developers wants to add null in where clause. 
            if (ReferenceEquals(closure , null))
            {
                sc.Type = SearchCondition.Types.Basic;
                sc.ParamName1 = "NULL";
            }
            else
            {
                Builder q = RunClosure(closure );
                sc.Type = SearchCondition.Types.SubQuery;
                sc.Query = q;
                MergeParameter(q.Parameters);
            }

            WhereClauses.Add(sc);
            return this;
        }

        public virtual Builder OrWhere(string xColumn, string xOperator, object xValue)
        {
            return Where(xColumn, xOperator, xValue, "OR");
        }

        public virtual Builder OrWhere(Action<Builder> closure )
        {
            return Where(closure , "OR");
        }

        public virtual Builder OrWhere(string xColumn, string xOperator, Action<Builder> xclosure )
        {
            return Where(xColumn, xOperator, xclosure , "OR");
        }

        public virtual Builder WhereBetween(string xColumn, object xValue1, object xValue2, string xBooelan = "AND", bool xNot = false)
        {
            if (WhereClauses.Count() == 0)
                xBooelan = "";

            var sc = new SearchCondition
            {
                Type = SearchCondition.Types.Between,
                Column1 = xColumn,
                Boolean = xBooelan,
                Not = xNot,
                ParamName1 = GetAndAddParamName(xValue1),
                ParamName2 = GetAndAddParamName(xValue2)
            };

            WhereClauses.Add(sc);
            return this;
        }

        public virtual Builder WhereNotBetween(string xColumn, object xValue1, object xValue2, string xBooelan = "AND")
        {
            return WhereBetween(xColumn, xValue1, xValue2, xBooelan, true);
        }

        public virtual Builder OrWhereBetween(string xColumn, object xValue1, object xValue2)
        {
            return WhereBetween(xColumn, xValue1, xValue2, "OR");
        }

        public virtual Builder OrWhereNotBetween(string xColumn, object xValue1, object xValue2)
        {
            return WhereNotBetween(xColumn, xValue1, xValue2, "OR");

        }

        public virtual Builder WhereExits(Action<Builder> closure , string xBooelan = "AND", bool xNot = false)
        {
            if (WhereClauses.Count() == 0)
                xBooelan = "";

            Builder q = RunClosure(closure );
            var sc = new SearchCondition
            {
                Type = SearchCondition.Types.Exits,
                Boolean = xBooelan,
                Query = q,
                Not = xNot
            };

            WhereClauses.Add(sc);
            MergeParameter(q.Parameters);
            return this;
        }

        public virtual Builder WhereNotExits(Action<Builder> closure )
        {
            return WhereExits(closure , "AND", true);
        }

        public virtual Builder OrWhereExits(Action<Builder> closure )
        {
            return WhereExits(closure , "OR", false);
        }

        public virtual Builder OrWhereNotExits(Action<Builder> closure )
        {
            return WhereExits(closure , "OR", true);
        }

        protected virtual Builder WhereIn(string xColumn, string xBooelan = "AND", bool xNot = false, params object[] xValues)
        {
            if (WhereClauses.Count() == 0)
                xBooelan = "";

            var sb = new StringBuilder();
            foreach (var value in xValues)
            {
                var param = GetAndAddParamName(value);
                sb.Append(param + ",");
            }

            var sc = new SearchCondition
            {
                Type = SearchCondition.Types.In,
                Column1 = xColumn,
                Boolean = xBooelan,
                Not = xNot,
                ParamName1 = sb.Remove(sb.Length - 1, 1).ToString()
            };

            WhereClauses.Add(sc);
            return this;
        }

        public virtual Builder WhereIn(string xColumn, params object[] xValues)
        {
            return WhereIn(xColumn, "AND", false, xValues);
        }

        public virtual Builder WhereNotIn(string xColumn, params object[] xValues)
        {
            return WhereIn(xColumn, "AND", true, xValues);
        }

        public virtual Builder OrWhereIn(string xColumn, params object[] xValues)
        {
            return WhereIn(xColumn, "OR", false, xValues);
        }

        public virtual Builder OrWhereNotIn(string xColumn, params object[] xValues)
        {
            return WhereIn(xColumn, "OR", true, xValues);
        }

        public virtual Builder WhereIn(string xColumn, Action<Builder> closure , string xBooelan = "AND", bool xNot = false)
        {
            if (WhereClauses.Count() == 0)
                xBooelan = "";

            Builder q = RunClosure(closure );

            var sc = new SearchCondition
            {
                Type = SearchCondition.Types.InSub,
                Column1 = xColumn,
                Boolean = xBooelan,
                Query = q
            };

            WhereClauses.Add(sc);
            MergeParameter(q.Parameters);
            return this;
        }

        public virtual Builder WhereNotIn(string xColumn, Action<Builder> closure )
        {
            return WhereIn(xColumn, closure , "AND", true);
        }

        public virtual Builder OrWherIn(string xColumn, Action<Builder> closure )
        {
            return WhereIn(xColumn, closure , "OR", false);
        }

        public virtual Builder OrWhereNotIn(string xColumn, Action<Builder> closure )
        {
            return WhereIn(xColumn, closure , "OR", true);
        }

        public virtual Builder Union(Builder builder, bool all = false, bool redefineParameter = true)
        {
            if (redefineParameter)
            {
                RedefineParameter(builder);
            }
            var sc = new SearchCondition
            {
                Type = SearchCondition.Types.Union,
                Not = all,
                Query = builder
            };
            MergeParameter(builder.Parameters);
            UnionClauses.Add(sc);
            return this;
        }

        public virtual Builder Union(Action<Builder> closure)
        {
            Builder q = RunClosure(closure);
            return Union(q, redefineParameter: false);
        }

        public virtual Builder UnionAll(Builder builder)
        {
            return Union(builder, true);
        }

        public virtual Builder UnionAll(Action<Builder> closure)
        {
            Builder q = RunClosure(closure);
            return Union(q, true, false);
        }

        public virtual Builder GroupBy(params string[] xGroup)
        {
            foreach (string c in xGroup)
            {
                Groups.Add(c);
            }
            return this;
        }

        public virtual Builder Having(string xColumn, string xOperator, object xValue, string xBoolean = "AND")
        {
            if (HavingClauses.Count() == 0)
                xBoolean = "";

            var sc = new SearchCondition
            {
                Boolean = xBoolean,
                Column1 = xColumn,
                Operator = WrapOperator(xOperator),
                ParamName1 = GetAndAddParamName(xValue),
                Type = SearchCondition.Types.Basic
            };
            HavingClauses.Add(sc);
            return this;
        }

        public virtual Builder Having(Action<Builder> closure , string booelan = "AND")
        {
            Builder q = NewBuilder();
            closure (q);

            var wm = new SearchCondition
            {
                Type = SearchCondition.Types.Nasted,
                Boolean = booelan,
                Query = q
            };

            HavingClauses.Add(wm);
            MergeParameter(q.Parameters);
            return this;
        }

        public virtual Builder Having(string xColumn, string xOperator, Action<Builder> closure , string xBooelan = "AND")
        {
            Builder q = RunClosure(closure );
            var wm = new SearchCondition
            {
                Type = SearchCondition.Types.SubQuery,
                Column1 = xColumn,
                Operator = WrapOperator(xOperator),
                Boolean = xBooelan,
                Query = q
            };
            HavingClauses.Add(wm);
            MergeParameter(q.Parameters);
            return this;
        }

        public virtual Builder OrHaving(Action<Builder> closure)
        {
            return Having(closure, "OR");
        }

        public virtual Builder OrHaving(string xColumn, string xOperator, Action<Builder> closure)
        {
            return Having(xColumn, xOperator, closure, "OR");
        }

        public virtual Builder OrHaving(string xColumn, string xOperator, object xValue)
        {
            return Having(xColumn, xOperator, xValue, "OR");
        }

        public virtual Builder OrderBy(string xColumn, string xDirection = "ASC")
        {
            OrderBy(xDirection, new string[] { xColumn });
            return this;
        }

        public virtual Builder OrderByDesc(params string[] xColumn)
        {
            return OrderBy("DESC", xColumn);
        }

        public virtual Builder OrderByAsc(params string[] xColumn)
        {
            return OrderBy("ASC", xColumn);
        }

        protected Builder OrderBy(string xDirection, string[] xColumn)
        {
            foreach (var column in xColumn)
            {
                Orders.Add(string.Format("{0} {1}", column, xDirection));
            }
            return this;
        }

        public Builder Take(int value)
        {
            if (value >= 0) {
                this.Fetch = value;
            }
            return this;
        }

        public Builder Skip(int value)
        {
            this.Offset = Math.Max(0, value);
            return this;
        }

        public virtual int ExecuteStatement(string sql, params object[] parameters)
        {
            SqlSyntax.CompileStatement(this, parameters);
            int result = DBProcessor.ExecuteNonQuery(sql, DbConfig,Parameters);
            return result;
        }

        public virtual int Insert(params object[] xValues)
        {
            // Every insert command will be treated like a batch insert.
            if (xValues == null || xValues.Length == 0)
                return 0;

            string sql = SqlSyntax.CompileInsert(this, xValues);
            var result = DBProcessor.ExecuteNonQuery(sql, DbConfig, Parameters);
            ClearParameters();
            ClearClauses();
            return result;
        }

        public virtual int Update(object xValues)
        {
            if (xValues == null)
                return 0;

            string sql = SqlSyntax.CompileUpdate(this, xValues);
            var result = DBProcessor.ExecuteNonQuery(sql, DbConfig, Parameters);
            ClearParameters();
            ClearClauses();
            return result;
        }

        public virtual int Delete(string column = null, object id = null, string xOperator = "=")
        {
            if (id != null && column != null)
                Where(column, WrapOperator(xOperator), id);

            var sql = SqlSyntax.CompileDelete(this);
            var result = DBProcessor.ExecuteNonQuery(sql, DbConfig, Parameters);
            ClearParameters();
            ClearClauses();
            return result;
        }

        public virtual int DeleteById(object id = null, string column = "id")
        {
            return Delete(column, id);
        }

        public virtual Builder Truncate()
        {
            var sql = SqlSyntax.CompileTruncate(this);
            var result = DBProcessor.ExecuteNonQuery(sql, DbConfig);
            return this;
        }

        public virtual DataSet Get()
        {
            var sql = ToSql();
            var ds = DBProcessor.ExecuteDataset(sql, DbConfig, Parameters);
            ClearParameters();
            ClearClauses();
            return ds;
        }

        public virtual IEnumerable<T> Get<T>()
        {
            var sql = ToSql();
            IEnumerable<T> data = DBProcessor.ExecuteQuery<T>(sql, DbConfig, Parameters);
            ClearParameters();
            ClearClauses();
            return data;
        }

        public virtual object GetValue()
        {
            var sql = ToSql();
            var value = DBProcessor.ExecuteScalar(sql, DbConfig, Parameters);
            ClearParameters();
            ClearClauses();
            return value;
        }

        public virtual string ToSql()
        {
            if (Columns == null || Columns.Count == 0)
                Columns.Add("*");

            return SqlSyntax.CompileSelect(this);
        }

        public virtual void MergeParameter(Dictionary<string, object> xParameters)
        {
            foreach (var param in xParameters)
            {
                object value;
                Parameters.TryGetValue(param.Key, out value);

                if (value == null)
                    Parameters.Add(param.Key, param.Value);
                else
                    Parameters[param.Key] = param.Value;
            }
        }

        public virtual string WrapOperator(string xOperators)
        {
            return SqlSyntax.WrapOperator(xOperators);
        }

        public virtual string WrapTable(string xValue, string xPrefix = null)
        {
            return SqlSyntax.WrapTable(xValue, xPrefix);
        }

        public virtual string WrapValue(string xValue)
        {
            return SqlSyntax.WrapValue(xValue);
        }

        public virtual void ClearParameters()
        {
            Counter = 0;
            Parameters.Clear();
        }

        public virtual void ClearClauses()
        {
            WhereClauses.Clear();
            UnionClauses.Clear();
            JoinClauses.Clear();
            HavingClauses.Clear();
            Orders.Clear();
            Columns.Clear();
            Groups.Clear();
            Table = null;
        }

        protected virtual Builder RunClosure(Action<Builder> closure )
        {
            Builder q = NewBuilder();
            q.Counter = Counter;
            q.DbConfig = DbConfig;
            closure (q);
            Counter = q.Counter;
            return q;
        }

        public static Raw Raw(object value)
        {
            return new Raw(value);
        }

        public string GetAndAddParamName(object value)
        {
            if (value is Raw)
                return value.ToString();

            string paramName = "@" + Counter++;
            Parameters.Add(paramName, value);
            return paramName;
        }

        public void SetParamName(string paramName, object value)
        {
            Parameters.Add(paramName, value);
        }

        protected void RedefineParameter(Builder builder)
        {
            for (int i = 0; i < builder.Counter; i++)
            {
                var findParam = "@" + i;
                var newParamPrefix = "@" + i + "00_";
                RedefineParameter(builder, findParam, builder.WhereClauses, newParamPrefix);
                RedefineParameter(builder, findParam, builder.HavingClauses, newParamPrefix);
                RedefineParameter(builder, findParam, builder.JoinClauses, newParamPrefix);
            }
        }

        protected bool RedefineParameter(Builder builder, string findParam, List<SearchCondition> clauses, string newParamPrefix)
        {
            foreach (var clause in clauses)
            {
                if (clause.ParamName1 == findParam)
                {
                    var value = builder.Parameters[findParam];
                    string newParamName = newParamPrefix + this.Counter++;
                    clause.ParamName1 = newParamName;
                    builder.Parameters.Remove(findParam);
                    builder.Parameters.Add(newParamName, value);
                    return true;
                }
                if (clause.ParamName2 == findParam)
                {
                    var value = builder.Parameters[findParam];
                    string newParamName = newParamPrefix + this.Counter++;
                    clause.ParamName2 = newParamName;
                    builder.Parameters.Remove(findParam);
                    builder.Parameters.Add(newParamName, value);
                    return true;
                }
            }
            return false;
        }
    }
}
