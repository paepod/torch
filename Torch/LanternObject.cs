﻿using System;
using System.Collections.Concurrent;
using Torch.Database;
using Torch.Database.Query;
using Torch.Config;
namespace Torch.Object
{
    public class LanternObject
    {
        protected static readonly object SyncRoot = new object();

        protected volatile MySqlSyntax _MySqlSyntax;
        public MySqlSyntax MySqlSyntax
        {
            get
            {
                if (_MySqlSyntax == null)
                {
                    lock (SyncRoot)
                    {
                        if (_MySqlSyntax == null)
                            _MySqlSyntax = new MySqlSyntax();
                    }
                }

                return _MySqlSyntax;
            }
        }

        protected volatile PostgresSyntax _PostgresSyntax;
        public PostgresSyntax PostgresSyntax
        {
            get
            {
                if (_PostgresSyntax == null)
                {
                    lock (SyncRoot)
                    {
                        if (_PostgresSyntax == null)
                            _PostgresSyntax = new PostgresSyntax();
                    }
                }

                return _PostgresSyntax;
            }
        }

        protected volatile SQLiteSyntax _SQLiteSyntax;
        public SQLiteSyntax SQLiteSyntax
        {
            get
            {
                if (_SQLiteSyntax == null)
                {
                    lock (SyncRoot)
                    {
                        if (_SQLiteSyntax == null)
                            _SQLiteSyntax = new SQLiteSyntax();
                    }
                }

                return _SQLiteSyntax;
            }
        }

        protected volatile SqlServerSyntax _SqlServerSyntax;
        public SqlServerSyntax SqlServerSyntax
        {
            get
            {
                if (_SqlServerSyntax == null)
                {
                    lock (SyncRoot)
                    {
                        if (_SqlServerSyntax == null)
                            _SqlServerSyntax = new SqlServerSyntax();
                    }
                }

                return _SqlServerSyntax;
            }
        }

        protected volatile BasicSqlSyntax _BasicSqlSyntax;
        public BasicSqlSyntax BasicSqlSyntax
        {
            get
            {
                if (_BasicSqlSyntax == null)
                {
                    lock (SyncRoot)
                    {
                        if (_BasicSqlSyntax == null)
                            _BasicSqlSyntax = new BasicSqlSyntax();
                    }
                }

                return _BasicSqlSyntax;
            }
        }

        public DbConfig DefaultDbConfig { get; set; }

        protected volatile ConcurrentDictionary<string, DbConfig> _DbConfigs;
        public ConcurrentDictionary<string, DbConfig> DbConfigs
        {
            get
            {
                lock (SyncRoot)
                {
                    if (_DbConfigs == null)
                        _DbConfigs = new ConcurrentDictionary<string, DbConfig>();
                    return _DbConfigs;
                }
            }
            set { _DbConfigs = value; }
        }

        public Builder Database(string table = null, Builder builder = null, Action<Builder> closure = null, string aliasName = null, string configName = "")
        {
            var self = this;
            DbConfig dbConfig = self.GetConfigByName(configName);
            BasicSqlSyntax syntax = self.GetSyntax(dbConfig);

            if (builder != null)
            {
                return new Builder(syntax, dbConfig).From(aliasName, builder);
            }
            if (closure != null)
            {
                return new Builder(syntax, dbConfig).From(aliasName, closure);
            }
            return new Builder(syntax, dbConfig).From(table);
        }

        public DbConfig GetConfigByName(string configName)
        {
            var self = this;
            DbConfig dbConfig = null;
            self.DbConfigs.TryGetValue(configName, out dbConfig);

            if (dbConfig == null && self.DefaultDbConfig != null)
            {
                dbConfig = self.DefaultDbConfig;
            }
            if (dbConfig == null && self.DefaultDbConfig == null)
            {
                dbConfig = new DbConfig();
                dbConfig.DbSyntax = DbSyntax.SqlServer;
                dbConfig.DbProvider = DbProvider.SqlServer;
            }

            return dbConfig;
        }

        public BasicSqlSyntax GetSyntax(DbConfig dbConfig)
        {
            BasicSqlSyntax syntax;
            var self = this;
            switch (dbConfig.DbSyntax)
            {
                case DbSyntax.MySql:
                    syntax = self.MySqlSyntax;
                    break;
                case DbSyntax.PostgreSQL:
                    syntax = self.PostgresSyntax;
                    break;
                case DbSyntax.SQLite:
                    syntax = self.SQLiteSyntax;
                    break;
                case DbSyntax.SqlServer:
                    syntax = self.SqlServerSyntax;
                    break;
                default:
                    syntax = self.BasicSqlSyntax;
                    break;
            }

            return syntax;
        }
    }
}
