﻿namespace Torch.Config
{
    public enum DbSyntax
    {
        Basic,
        MySql,
        PostgreSQL,
        SqlServer,
        SQLite
    }


    public class DbConfig
    {
        public string Name { get; set; }
        public string ConnectionString { get; set; }
        public DbProvider DbProvider { get; set; }
        public bool IsDefault { get; set; }
        public DbSyntax DbSyntax { get; set; }

        public DbConfig(DbConfig xDbConfig) : this(xDbConfig.Name, xDbConfig.ConnectionString,
            xDbConfig.DbProvider, xDbConfig.DbSyntax, xDbConfig.IsDefault)
        {
        }

        public DbConfig(string xConfigName, string xConnectionString,
            DbProvider xDbProvider, DbSyntax xDbSyntax, bool isDefault = false)
        {
            this.IsDefault = isDefault;
            this.Name = xConfigName;
            this.ConnectionString = xConnectionString;
            this.DbProvider = xDbProvider;
            this.DbSyntax = xDbSyntax;
        }

        public DbConfig()
        {
        }
    }

    public class DbProvider
    {
        public readonly string Name;

        private DbProvider(string providerName)
        {
            this.Name = providerName;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static DbProvider MySql
        {
            get
            {
                return new DbProvider("MySql.Data.MySqlClient");
            }
        }

        public static DbProvider PostgreSql
        {
            get
            {
                return new DbProvider("Npgsql");
            }
        }

        public static DbProvider SQLite
        {
            get
            {
                return new DbProvider("System.Data.SQLite");
            }
        }

        public static DbProvider SqlServer
        {
            get
            {
                return new DbProvider("System.Data.SqlClient");
            }
        }

        public static DbProvider Odbc
        {
            get
            {
                return new DbProvider("System.Data.Odbc");
            }
        }

        public static DbProvider OleDb
        {
            get
            {
                return new DbProvider("System.Data.OleDb");
            }
        }


    }


}
