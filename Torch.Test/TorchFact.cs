﻿using Xunit;
using Torch.Config;
using Torch.Database;
using System;
using System.IO;
using System.Linq;
using Torch.Object;

namespace Torch.Test
{
    public class TorchFact
    {
        [Fact]
        public void TestTorchDb()
        {
          
            Lantern.SetTestingInstance(new LanternObject());

            var builder = Lantern.DB("TEST");
            Action<Builder> closure = (Builder b) => { b.Select("*").From("TEST"); };
            Assert.Equal("SELECT * FROM TEST", Lantern.DB("TEST").ToSql());

            Assert.Equal("SELECT * FROM (SELECT * FROM TEST)", Lantern.DB(builder).ToSql());
            Assert.Equal("SELECT * FROM (SELECT * FROM TEST) AS builder", Lantern.DB("    builder    ", builder).ToSql());

            Assert.Equal("SELECT * FROM (SELECT * FROM TEST)", Lantern.DB(closure).ToSql());
            Assert.Equal("SELECT * FROM (SELECT * FROM TEST) AS closure", Lantern.DB("    closure    ", closure).ToSql());
        }

        [Fact]
        public void TestDefaultDbConfig()
        {
            Lantern.SetTestingInstance(new LanternObject());

            SetSqlServer("data1", false);
            var sqlconfig = Lantern.GetDBConfig("data1");
            Assert.True(sqlconfig != null);

        }

        [Fact]
        public void TestChangeDbConfig()
        {
            Lantern.SetTestingInstance(new LanternObject());

            var configName1 = "sql1";
            var configName2 = "sql2";
            var configName3 = "mysql";
            var configName4 = "sqlite";
            var configName5 = "pgsql";

            SetSqlServer(configName1, true);
            SetSqlServer(configName2);
            SetMySql(configName3);
            SetSQLite(configName4);
            SetPgSql(configName5);

            var sqlconfig1 = Lantern.GetDBConfig(configName1);
            var sqlconfig2 = Lantern.GetDBConfig(configName2);
            var mysqlconfig2 = Lantern.GetDBConfig(configName3);
            var sqlieconfig2 = Lantern.GetDBConfig(configName4);
            var pgsqlconfig2 = Lantern.GetDBConfig(configName5);


            Assert.True(sqlconfig1 == Lantern.Instance.DefaultDbConfig);

            Lantern.SetDefaultDBConfig(sqlconfig2.Name);
            Assert.True(sqlconfig2 == Lantern.Instance.DefaultDbConfig);
            Assert.Equal("MySql.Data.MySqlClient", mysqlconfig2.DbProvider.Name);
            Assert.Equal("System.Data.SqlClient", sqlconfig1.DbProvider.Name);
            Assert.Equal("System.Data.SQLite", sqlieconfig2.DbProvider.Name);
            Assert.Equal("Npgsql", pgsqlconfig2.DbProvider.Name);

        }

        private void SetSqlServer(string configName, bool isDefault = false)
        {
            var connStr = @"";
            Lantern.AddDBConfig(configName, connStr, DbProvider.SqlServer, DbSyntax.SqlServer);
        }

        private void SetMySql(string configName)
        {
            var connStr = "";
            Lantern.AddDBConfig(configName, connStr, DbProvider.MySql, DbSyntax.MySql);
        }

        private void SetPgSql(string configName)
        {
            var connStr = "";
            Lantern.AddDBConfig(configName, connStr, DbProvider.PostgreSql, DbSyntax.PostgreSQL);
        }

        private void SetSQLite(string configName)
        {
            var connStr = "";
            Lantern.AddDBConfig(configName, connStr, DbProvider.SQLite, DbSyntax.SQLite);
        }


        #region RealTest

        [Fact]
        public void TestExecuteStatement()
        {
            Lantern.SetTestingInstance(new LanternObject());
            var configname = "sqlserver2";
            SqlServerSetUp(configname);

            Lantern.DBExecuteStatement("IF OBJECT_ID('Rainbows', 'U') IS NOT NULL DROP TABLE Rainbows;");
            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('Strangers', 'U') IS NOT NULL DROP TABLE Strangers;");
            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('Friends', 'U') IS NOT NULL DROP TABLE Friends;");
            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID(@Visitors, 'U') IS NOT NULL DROP TABLE Visitors;", new { @Visitors = "Visitors" });

            Lantern.DBConnection(configname).ExecuteStatement("CREATE TABLE Friends(Flavor int, Color int);");
            Lantern.DBConnection(configname).ExecuteStatement("CREATE TABLE Strangers(Flavor int, StrangerID int);");
            Lantern.DBConnection(configname).ExecuteStatement("CREATE TABLE Rainbows(StrangerID int, Color int, Wavelength int);");
            Lantern.DBConnection(configname).ExecuteStatement("CREATE TABLE Visitors(StrangerID int, Color int, Wavelength int);");

        }

        [Fact]
        public void TestSqlServerDelete()
        {
            Lantern.SetTestingInstance(new LanternObject());
            var configname = "sqlserver";
            SqlServerSetUp(configname);

            var createTable = "CREATE TABLE MyDelete(Flavor int);";
            var dropTable = "IF OBJECT_ID('MyDelete', 'U') IS NOT NULL DROP TABLE MyDelete;";

            Lantern.DBConnection(configname).ExecuteStatement(dropTable);
            Lantern.DBConnection(configname).ExecuteStatement(createTable);

            var builder = Lantern.DB("test", configname);

            Lantern.DB("MyDelete", configname).Delete();
            Lantern.DB("MyDelete", configname).Truncate();
            Lantern.DBConnection(configname).ExecuteStatement(dropTable);
        }



        [Fact]
        public void TestSqlServerComplex()
        {
            Lantern.SetTestingInstance(new LanternObject());

            var configname = "sqlserver";
            SqlServerSetUp(configname);

            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('AFriend', 'U') IS NOT NULL DROP TABLE AFriend; CREATE TABLE AFriend(id int, Amount int , Messages NVARCHAR(255));");
            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('AStranger', 'U') IS NOT NULL DROP TABLE AStranger; CREATE TABLE AStranger(id int, afriendId int, Amount int, Messages NVARCHAR(255));");
            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('MyProc', 'P') IS NOT NULL DROP PROC MyProc;");

            var data1 = new object[99];
            var data2 = new object[99];
            var rng = new Random();

            for (int i = 0; i < 99; i++)
                data1[i] = new { id = i, Amount = rng.Next(10000), Messages = Path.GetRandomFileName().Replace(".", "") };

            for (int i = 0; i < 99; i++)
                data2[i] = new { id = i, afriendId = rng.Next(99), Amount = rng.Next(10000), Messages = Path.GetRandomFileName().Replace(".", "") };

            int result;

            result = Lantern.DB("AFriend", configname).Insert(data1);
            Assert.Equal(99, result);
            result = Lantern.DB("AStranger", configname).Insert(data2);
            Assert.Equal(99, result);

            result = Lantern.DB("AStranger", configname).DeleteById(2);
            Assert.Equal(1, result);

            var count = Lantern.DB("AFriend", configname).Select("COUNT(*)").From("test").GetValue();
            Assert.Equal(101, count);

            var builder1 = Lantern.DB("AFriend", configname).Where("id", "<", 4);
            var ds = Lantern.DB("AFriend", configname).WhereBetween("id", 5, 10).UnionAll(builder1).Get();
            Assert.Equal(10, ds.Tables[0].Rows.Count);

            result = Lantern.DB("AStranger", configname).Where("id", "=", 44).Update(new { Amount = 9999, Messages = "3" });
            Assert.Equal(1, result);

            var aStranger = Lantern.DB("AStranger").Where("id", 44).Get<AStranger>().Single();
            Assert.Equal(44, aStranger.id);
            Assert.Equal(9999, aStranger.Amount);
            Assert.Equal("3", aStranger.Messages);

            aStranger.Amount = 555;
            aStranger.Messages = "555";

            result = Lantern.DB("AStranger", configname).Where("id", "=", 44).Update(aStranger);

            var aStranger2 = Lantern.DB("AStranger").Where("id", 44).Get<dynamic>().Single();
            Assert.Equal(44, aStranger2.id);
            Assert.Equal(555, aStranger2.Amount);
            Assert.Equal("555", aStranger2.Messages);

            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('AFriend', 'U') IS NOT NULL DROP TABLE AFriend;");
            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('AStranger', 'U') IS NOT NULL DROP TABLE AStranger;");

        }

        [Fact]
        public void TestSQLiteDelete()
        {
            var configname = "sqlite";
            SQLiteSetup(configname);
            Lantern.DB("TestDelete", configname).Delete();
        }

        [Fact]
        public void TestSQLiteComplex()
        {
            Lantern.SetTestingInstance(new LanternObject());

            var configname = "sqlite";
            SqlServerSetUp(configname);

            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('AFriend', 'U') IS NOT NULL DROP TABLE AFriend; CREATE TABLE AFriend(id int, Amount int , Messages NVARCHAR(255));");
            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('AStranger', 'U') IS NOT NULL DROP TABLE AStranger; CREATE TABLE AStranger(id int, afriendId int, Amount int, Messages NVARCHAR(255));");

            var data1 = new object[99];
            var data2 = new object[99];
            var rng = new Random();

            for (int i = 0; i < 99; i++)
                data1[i] = new { id = i, Amount = rng.Next(10000), Messages = Path.GetRandomFileName().Replace(".", "") };

            for (int i = 0; i < 99; i++)
                data2[i] = new { id = i, afriendId = rng.Next(99), Amount = rng.Next(10000), Messages = Path.GetRandomFileName().Replace(".", "") };

            int result;

            result = Lantern.DB("AFriend", configname).Insert(data1);
            Assert.Equal(99, result);
            result = Lantern.DB("AStranger", configname).Insert(data2);
            Assert.Equal(99, result);

            result = Lantern.DB("AStranger", configname).DeleteById(2);
            Assert.Equal(1, result);

            var count = Lantern.DB("AFriend", configname).Select("COUNT(*)").From("test").GetValue();
            Assert.Equal(101, count);

            var builder1 = Lantern.DB("AFriend", configname).Where("id", "<", 4);
            var ds = Lantern.DB("AFriend", configname).WhereBetween("id", 5, 10).UnionAll(builder1).Get();
            Assert.Equal(10, ds.Tables[0].Rows.Count);

            result = Lantern.DB("AStranger", configname).Where("id", "=", 44).Update(new { Amount = 9999, Messages = "3" });
            Assert.Equal(1, result);

            var aStranger = Lantern.DB("AStranger").Where("id", 44).Get<AStranger>().Single();
            Assert.Equal(44, aStranger.id);
            Assert.Equal(9999, aStranger.Amount);
            Assert.Equal("3", aStranger.Messages);

            aStranger.Amount = 555;
            aStranger.Messages = "555";

            result = Lantern.DB("AStranger", configname).Where("id", "=", 44).Update(aStranger);

            var aStranger2 = Lantern.DB("AStranger").Where("id", 44).Get<dynamic>().Single();
            Assert.Equal(44, aStranger2.id);
            Assert.Equal(555, aStranger2.Amount);
            Assert.Equal("555", aStranger2.Messages);

            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('AFriend', 'U') IS NOT NULL DROP TABLE AFriend;");
            Lantern.DBConnection(configname).ExecuteStatement("IF OBJECT_ID('AStranger', 'U') IS NOT NULL DROP TABLE AStranger;");

        }

        private void SqlServerSetUp(string configName)
        {
            var connStr = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=datatest;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            Lantern.AddDBConfig(configName, connStr, DbProvider.SqlServer, DbSyntax.SqlServer);
        }

        private void SQLiteSetup(string configName)
        {
            var connStr = @"Data Source=D:\test1.db3;Version=3;";
            Lantern.AddDBConfig(configName, connStr, DbProvider.SQLite, DbSyntax.SQLite);
        }


        public class AStranger
        {
            public int id { get; set; }
            public int afriendId { get; set; }
            public int Amount { get; set; }
            public string Messages { get; set; }
        }
    }

    #endregion

}
