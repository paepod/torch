﻿using Xunit;
using Torch.Database;
using Torch.Database.Query;

namespace Torch.Tests.Database
{
    public class QueryBuilderFact
    {
        #region Query Builder

        [Fact]
        public void TestEmptyFrom()
        {
            var builder = GetBuilder();
            builder.From("");
            Assert.Equal("SELECT * FROM", builder.ToSql());
        }

        [Fact]
        public void TestFromSubQuery()
        {
            var builder = GetBuilder();
            builder.Select("*").From("OKAY", (Builder q) =>
             {
                 q.Select("A").From("B").Where("id", 1);
             });
            Assert.Equal("SELECT * FROM (SELECT A FROM B WHERE id = @0) AS OKAY", builder.ToSql());

            builder = GetBuilder();
            var builder2 = GetBuilder();
            builder.Select("*").From("OKAY", builder2.From("B").Where("id", 1));
            Assert.Equal("SELECT * FROM (SELECT * FROM B WHERE id = @000_0) AS OKAY", builder.ToSql());

            builder = GetBuilder();
            builder2 = GetBuilder();
            builder.Select("*").From("", builder2.From("B").Where("id", 1));
            Assert.Equal("SELECT * FROM (SELECT * FROM B WHERE id = @000_0)", builder.ToSql());
        }

        [Fact]
        public void TestBasicSelect()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers");
            Assert.Equal("SELECT * FROM customers", builder.ToSql());
        }

        [Fact]
        public void TestSelectWithOneColumn()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers");
            Assert.Equal("SELECT FirstName FROM customers", builder.ToSql());
        }

        [Fact]
        public void TestSelectWithTwoColumns()
        {
            var builder = GetBuilder();
            builder.Select("FirstName", "LastName").From("customers");
            Assert.Equal("SELECT FirstName, LastName FROM customers", builder.ToSql());
        }

        [Fact]
        public void TestAddSelect()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").Select("LastName").From("customers");
            Assert.Equal("SELECT FirstName, LastName FROM customers", builder.ToSql());
        }

        [Fact]
        public void TestAddSelectSubQuery()
        {
            var builder = GetBuilder();
            builder.From("customers").Select("FirstName").Select("LastName").Select("AliasName", (Builder b) => { b.Select("COUNT(*)").From("B").Where("id", 1); });
            Assert.Equal("SELECT FirstName, LastName, (SELECT COUNT(*) FROM B WHERE id = @0) AS AliasName FROM customers", builder.ToSql());
        }

        [Fact]
        public void TestBasicWhere()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "=", 1);
            Assert.Equal("SELECT FirstName FROM customers WHERE id = @0", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
        }

        [Fact]
        public void TestWhereAnd()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "=", 1).Where("Firstname", "=", "Austin");
            Assert.Equal("SELECT FirstName FROM customers WHERE id = @0 AND Firstname = @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
            Assert.Equal(builder.Parameters["@1"], "Austin");
        }

        [Fact]
        public void TestWhereOr()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "=", 1).OrWhere("Firstname", "=", "Austin");
            Assert.Equal("SELECT FirstName FROM customers WHERE id = @0 OR Firstname = @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
            Assert.Equal(builder.Parameters["@1"], "Austin");
        }

        [Fact]
        public void TestWhereAndOr()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "=", 1).OrWhere("Firstname", "=", "Austin");
            Assert.Equal("SELECT FirstName FROM customers WHERE id = @0 OR Firstname = @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
            Assert.Equal(builder.Parameters["@1"], "Austin");
        }

        [Fact]
        public void TestWhereNested()
        {
            var builder = GetBuilder();
            builder
                .Select("FirstName")
                .From("customers")
                .Where("id", "=", 1)
                .OrWhere((Builder b) =>
                {
                    b.Where("Firstname", "=", "Austin").Where("Lastname", "=", "Power");
                });
            Assert.Equal("SELECT FirstName FROM customers WHERE id = @0 OR (Firstname = @1 AND Lastname = @2)",
                builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
            Assert.Equal(builder.Parameters["@1"], "Austin");
            Assert.Equal(builder.Parameters["@2"], "Power");

        }

        [Fact]
        public void TestWhereBetween()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers").WhereBetween("id", 1, 2);
            Assert.Equal("SELECT FirstName FROM customers WHERE id BETWEEN @0 AND @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
            Assert.Equal(builder.Parameters["@1"], 2);

            builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "<", 3).OrWhereBetween("id", 1, 2);
            Assert.Equal("SELECT FirstName FROM customers WHERE id < @0 OR id BETWEEN @1 AND @2", builder.ToSql());

        }

        [Fact]
        public void TestWhereIn()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers").WhereIn("id", 1, 2);
            Assert.Equal("SELECT FirstName FROM customers WHERE id IN (@0,@1)", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
            Assert.Equal(builder.Parameters["@1"], 2);

            builder = GetBuilder();
            builder.Select("FirstName").From("customers").WhereNotIn("id", 1, 2).Where("id", "<", 3);
            Assert.Equal("SELECT FirstName FROM customers WHERE id NOT IN (@0,@1) AND id < @2", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
            Assert.Equal(builder.Parameters["@1"], 2);
            Assert.Equal(builder.Parameters["@2"], 3);

            builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "<", 3).OrWhereIn("id", 1, 2);
            Assert.Equal("SELECT FirstName FROM customers WHERE id < @0 OR id IN (@1,@2)", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 3);
            Assert.Equal(builder.Parameters["@1"], 1);
            Assert.Equal(builder.Parameters["@2"], 2);

            builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "<", 3).OrWhereNotIn("id", 1, 2);
            Assert.Equal("SELECT FirstName FROM customers WHERE id < @0 OR id NOT IN (@1,@2)", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 3);
            Assert.Equal(builder.Parameters["@1"], 1);
            Assert.Equal(builder.Parameters["@2"], 2);

        }

        [Fact]
        public void TestWhereInSubQuery()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers").WhereIn("id", (Builder b) => { b.From("test").Where("id", "<", 10); }).Where("id", "<", 3);
            Assert.Equal("SELECT FirstName FROM customers WHERE id IN (SELECT * FROM test WHERE id < @0) AND id < @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 10);
            Assert.Equal(builder.Parameters["@1"], 3);

            builder = GetBuilder();
            builder.Select("FirstName").From("customers").WhereIn("id", (Builder b) => { b.From("test").Where("id", "<", 10); }).OrWhere("id", "<", 3);
            Assert.Equal("SELECT FirstName FROM customers WHERE id IN (SELECT * FROM test WHERE id < @0) OR id < @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 10);
            Assert.Equal(builder.Parameters["@1"], 3);
        }

        [Fact]
        public void TestWhereExits()
        {
            var builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "<", 3).WhereNotExits((Builder b) => { b.From("test").Where("id", "<", 10); });
            Assert.Equal("SELECT FirstName FROM customers WHERE id < @0 AND NOT EXITS (SELECT * FROM test WHERE id < @1)", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 3);
            Assert.Equal(builder.Parameters["@1"], 10);

            builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "<", 3).OrWhereNotExits((Builder b) => { b.From("test").Where("id", "<", 10); });
            Assert.Equal("SELECT FirstName FROM customers WHERE id < @0 OR NOT EXITS (SELECT * FROM test WHERE id < @1)", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 3);
            Assert.Equal(builder.Parameters["@1"], 10);

            builder = GetBuilder();
            builder.Select("FirstName").From("customers").Where("id", "<", 3).OrWhereExits((Builder b) => { b.From("test").Where("id", "<", 10); });
            Assert.Equal("SELECT FirstName FROM customers WHERE id < @0 OR EXITS (SELECT * FROM test WHERE id < @1)", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 3);
            Assert.Equal(builder.Parameters["@1"], 10);


            builder = GetBuilder();
            builder.Select("FirstName").From("customers").WhereExits((Builder b) => { b.From("test").Where("id", "<", 10); }).OrWhere("id", "<", 3);
            Assert.Equal("SELECT FirstName FROM customers WHERE EXITS (SELECT * FROM test WHERE id < @0) OR id < @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 10);
            Assert.Equal(builder.Parameters["@1"], 3);

        }


        [Fact]
        public void TestBasicWhereNullAndNotNull()
        {
            var builder = GetBuilder();
            builder.From("customers").Where("updated_date", "is", null);
            Assert.Equal("SELECT * FROM customers WHERE updated_date IS NULL", builder.ToSql());
            Assert.Equal(builder.Parameters.Count, 0);

            builder = GetBuilder();
            builder.From("customers").Where("updated_date", "IS NOT", null).WhereBetween("id", 1, 2);
            Assert.Equal("SELECT * FROM customers WHERE updated_date IS NOT NULL AND id BETWEEN @0 AND @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 1);
            Assert.Equal(builder.Parameters["@1"], 2);

            builder = GetBuilder();
            builder.From("customers").WhereBetween("id", 1, 2).Where("updated_date", "is NOT ", null);
            Assert.Equal("SELECT * FROM customers WHERE id BETWEEN @0 AND @1 AND updated_date IS NOT NULL", builder.ToSql());

            builder = GetBuilder();
            builder.From("customers").WhereBetween("id", 1, 2).OrWhere("updated_date", " Is", null);
            Assert.Equal("SELECT * FROM customers WHERE id BETWEEN @0 AND @1 OR updated_date IS NULL", builder.ToSql());
        }

        [Fact]
        public void TestWhereSubQuery()
        {
            var builder = GetBuilder();

            builder
                .Select("Firstname", "Lastname")
                .From("customers")
                .Where("Email", "=", "1234")
                .OrWhere("id", "=", (Builder b) =>
                {
                    b.Select("max(id)").From("customers").Where("Email", "=", "testmail");
                });
            Assert.Equal("SELECT Firstname, Lastname FROM customers WHERE Email = @0 " +
                "OR (id = (SELECT max(id) FROM customers WHERE Email = @1))",
                builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], "1234");
            Assert.Equal(builder.Parameters["@1"], "testmail");
            object value;
            builder.Parameters.TryGetValue("@2", out value);
            Assert.Equal(value, null);


            builder = GetBuilder();

            builder
                .Select("Firstname", "Lastname")
                .From("customers")
                .Where("Email", "=", "1234")
                .Where("id", "=", (Builder b) =>
                {
                    b.Select("max(id)").From("customers").Where("Email", "=", "testmail");
                })
                .Where("Firstname", "LIKE", "%PAE%");
            Assert.Equal("SELECT Firstname, Lastname FROM customers WHERE Email = @0 " +
                "AND (id = (SELECT max(id) FROM customers WHERE Email = @1)) AND Firstname LIKE @2",
                builder.ToSql());


            Assert.Equal(builder.Parameters["@0"], "1234");
            Assert.Equal(builder.Parameters["@1"], "testmail");
            Assert.Equal(builder.Parameters["@2"], "%PAE%");
        }

        [Fact]
        public void TestWhereSubQuery_OrderBy()
        {
            var builder = GetBuilder();

            builder
                .Select("Firstname", "Lastname")
                .From("customers")
                .Where("Email", "=", "1234")
                .OrWhere("id", "=", (Builder b) =>
                {
                    b.Select("max(id)").From("customers").Where("Email", "=", "testmail");
                })
                .OrderByDesc("id");
            Assert.Equal("SELECT Firstname, Lastname FROM customers WHERE Email = @0 " +
                "OR (id = (SELECT max(id) FROM customers WHERE Email = @1)) ORDER BY id DESC",
                builder.ToSql());


            Assert.Equal(builder.Parameters["@0"], "1234");
            Assert.Equal(builder.Parameters["@1"], "testmail");
        }

        [Fact]
        public void TestWhereGroupBy()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers").Where("id", "=", "1").GroupBy("id", "Email");
            Assert.Equal("SELECT * FROM customers WHERE id = @0 GROUP BY id, Email", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], "1");
        }

        [Fact]
        public void TestUnions()
        {
            Builder builder1;

            builder1 = GetBuilder();
            var builder2 = builder1.NewBuilder();
            builder1.Select("*").From("customers").Where("id", "=", "1");
            builder2.Select("*").From("customers").Where("id", "=", "2").Union(builder1);
            Assert.Equal("SELECT * FROM customers WHERE id = @0 UNION SELECT * FROM customers WHERE id = @000_1", builder2.ToSql());
            Assert.Equal(builder2.Parameters["@0"], "2");
            Assert.Equal(builder2.Parameters["@000_1"], "1");

            builder1 = GetBuilder();
            builder2 = builder1.NewBuilder();
            var builder3 = builder1.NewBuilder();
            builder1.Select("*").From("customers").Where("id", "=", "1");
            builder2.Select("*").From("customers").Where("id", "=", "2").Having("id", "=", "3");
            builder3.Select("*").From("customers").Where("id", "=", "4").Union(builder2).Union(builder1).GroupBy("id", "Email");
            Assert.Equal("SELECT * FROM customers WHERE id = @0 UNION SELECT * FROM customers WHERE id = @000_1 HAVING id = @100_2 " +
                "UNION SELECT * FROM customers WHERE id = @000_3 GROUP BY id, Email", builder3.ToSql());
            Assert.Equal(builder3.Parameters["@0"], "4");
            Assert.Equal(builder3.Parameters["@000_1"], "2");
            Assert.Equal(builder3.Parameters["@100_2"], "3");
            Assert.Equal(builder3.Parameters["@000_3"], "1");


            builder1 = GetBuilder();
            builder1.Select("*").From("customers").Where("id", "=", "1")
                .Union((Builder b) => b.Select("*").From("customers").Where("id", "=", "2"));
            Assert.Equal("SELECT * FROM customers WHERE id = @0 UNION SELECT * FROM customers WHERE id = @1", builder1.ToSql());
            Assert.Equal(builder1.Parameters["@0"], "1");
            Assert.Equal(builder1.Parameters["@1"], "2");

        }

        [Fact]
        public void TestUnionAll()
        {
            Builder builder1;
            builder1 = GetBuilder();
            var builder2 = builder1.NewBuilder();
            builder1.Select("*").From("customers").Where("id", "=", "1");
            builder2.Select("*").From("customers").Where("id", "=", "3").UnionAll(builder1);
            Assert.Equal("SELECT * FROM customers WHERE id = @0 UNION ALL SELECT * FROM customers WHERE id = @000_1", builder2.ToSql());
            Assert.Equal(builder2.Parameters["@0"], "3");
            Assert.Equal(builder2.Parameters["@000_1"], "1");

            builder1 = GetBuilder();
            builder1.Select("*").From("customers").Where("id", "=", "1")
                .UnionAll((Builder b) => b.Select("*").From("customers").Where("id", "=", "2"));
            Assert.Equal("SELECT * FROM customers WHERE id = @0 UNION ALL SELECT * FROM customers WHERE id = @1", builder1.ToSql());
            Assert.Equal(builder1.Parameters["@0"], "1");
            Assert.Equal(builder1.Parameters["@1"], "2");

            builder1 = GetBuilder();
            builder2 = builder1.NewBuilder();
            var builder3 = builder1.NewBuilder();
            builder1.Select("*").From("customers").Where("id", "=", "1");
            builder2.Select("*").From("customers").Where("id", "=", "2").Where("id", "=", "4");
            builder3.Select("*").From("customers").Where("id", "=", "3").UnionAll(builder2).Union(builder1, true).GroupBy("id", "Email");
            Assert.Equal("SELECT * FROM customers WHERE id = @0 UNION ALL SELECT * FROM customers WHERE id = @000_1 AND id = @100_2 " +
                "UNION ALL SELECT * FROM customers WHERE id = @000_3 GROUP BY id, Email", builder3.ToSql());
            Assert.Equal(builder3.Parameters["@0"], "3");
            Assert.Equal(builder3.Parameters["@000_1"], "2");
            Assert.Equal(builder3.Parameters["@100_2"], "4");
            Assert.Equal(builder3.Parameters["@000_3"], "1");
        }

        [Fact]
        public void TestGroupBy()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers").GroupBy("id", "Email");
            Assert.Equal("SELECT * FROM customers GROUP BY id, Email", builder.ToSql());

            builder = GetBuilder();
            builder.Select("*").From("customers").GroupBy(new string[] { "id", "Email" });
            Assert.Equal("SELECT * FROM customers GROUP BY id, Email", builder.ToSql());
        }


        [Fact]
        public void TestOrderby()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers").OrderBy("Email").OrderByDesc("Age", "Phone").OrderByAsc("Data1", "Data2");
            Assert.Equal("SELECT * FROM customers ORDER BY Email ASC, Age DESC, Phone DESC, Data1 ASC, Data2 ASC", builder.ToSql());
        }

        [Fact]
        public void TestGroupByOrderby()
        {
            var builder = GetBuilder();
            builder.From("customers").GroupBy("id", "Email").OrderBy("Email").OrderByDesc("Age", "Phone").OrderByAsc("Data1", "Data2");
            Assert.Equal("SELECT * FROM customers GROUP BY id, Email " +
                "ORDER BY Email ASC, Age DESC, Phone DESC, Data1 ASC, Data2 ASC", builder.ToSql());
        }


        [Fact]
        public void TestHaving()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers").Having("SUM(NetIncome)", ">=", 5000);
            Assert.Equal("SELECT * FROM customers HAVING SUM(NetIncome) >= @0", builder.ToSql());

        }

        [Fact]
        public void TestHavingAnd()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers").Having("SUM(NetIncome)", ">=", 5000).Having("SUM(Salary)", ">", 500);
            Assert.Equal("SELECT * FROM customers HAVING SUM(NetIncome) >= @0 AND SUM(Salary) > @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 5000);
            Assert.Equal(builder.Parameters["@1"], 500);
        }

        [Fact]
        public void TestHavingOr()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers").Having("SUM(NetIncome)", ">=", 5000).OrHaving("SUM(Salary)", ">", 500);
            Assert.Equal("SELECT * FROM customers HAVING SUM(NetIncome) >= @0 OR SUM(Salary) > @1", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 5000);
            Assert.Equal(builder.Parameters["@1"], 500);
        }

        [Fact]
        public void TestHavinAndOr()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers")
                .Having("SUM(NetIncome)", ">=", 5000)
                .Having("SUM(Salary)", ">", 500)
                .OrHaving("SUM(Salary)", ">", 500);
            Assert.Equal("SELECT * FROM customers HAVING SUM(NetIncome) >= @0 AND SUM(Salary) > @1 OR SUM(Salary) > @2", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 5000);
            Assert.Equal(builder.Parameters["@1"], 500);
            Assert.Equal(builder.Parameters["@2"], 500);
        }

        [Fact]
        public void TestGroupByHaving()
        {
            var builder = GetBuilder();
            builder.Select("*").From("customers").GroupBy("id", "Email").Having("SUM(Salary)", ">", 500);
            Assert.Equal("SELECT * FROM customers GROUP BY id, Email HAVING SUM(Salary) > @0", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 500);
        }

        [Fact]
        public void TestGroupByHavingOrderBy()
        {
            var builder = GetBuilder();
            builder.Select("*")
                .From("customers")
                .GroupBy("id", "Email")
                .Having("SUM(Salary)", ">", 500)
                .OrderBy("Email")
                .OrderByDesc("Age", "Phone")
                .OrderByAsc("Data1", "Data2");
            Assert.Equal("SELECT * FROM customers GROUP BY id, Email HAVING SUM(Salary) > @0 " +
                "ORDER BY Email ASC, Age DESC, Phone DESC, Data1 ASC, Data2 ASC", builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], 500);
        }

        [Fact]
        public void TestHavingSubQuery()
        {
            var builder = GetBuilder()
                .Select("*")
                .From("customers")
                .Where("Email", "=", "1234")
                .Having("SUM(Salary)", ">", 500)
                .Having("SUM(Salary)", ">", (Builder b) =>
                {
                    b.Select("SUM(INCOME)").From("customers").Where("Email", "=", "testmail");
                });

            Assert.Equal("SELECT * FROM customers WHERE Email = @0 HAVING SUM(Salary) > @1 AND " +
                "(SUM(Salary) > (SELECT SUM(INCOME) FROM customers WHERE Email = @2))",
                builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], "1234");
            Assert.Equal(builder.Parameters["@1"], 500);
            Assert.Equal(builder.Parameters["@2"], "testmail");


            builder = GetBuilder()
                .From("customers")
                .Where("Email", "=", "1234")
                .Having("SUM(Salary)", ">", 500)
                .Having("SUM(Salary)", "=", (Builder b) =>
                {
                    b.Select("SUM(INCOME)").From("customers").Where("Email", "=", "testmail");
                })
                .OrHaving("COUNT(*)", ">", 1000);

            Assert.Equal("SELECT * FROM customers WHERE Email = @0 HAVING SUM(Salary) > @1 AND " +
                "(SUM(Salary) = (SELECT SUM(INCOME) FROM customers WHERE Email = @2)) OR COUNT(*) > @3",
                builder.ToSql());
            Assert.Equal(builder.Parameters["@0"], "1234");
            Assert.Equal(builder.Parameters["@1"], 500);
            Assert.Equal(builder.Parameters["@2"], "testmail");
            Assert.Equal(builder.Parameters["@3"], 1000);
        }
        [Fact]
        public void TestBasicJoins()
        {
            var builder = GetBuilder();
            builder.From("users").InnerJoin("contacts", "users.id", "contacts.id")
                .LeftJoin("photos", "users.id", "photos.id")
                .RightJoin("photos", "users.id", "photos.id", xOperator: "<>");
            Assert.Equal("SELECT * FROM users INNER JOIN contacts ON (users.id = contacts.id) " +
                "LEFT JOIN photos ON (users.id = photos.id) RIGHT JOIN photos ON (users.id <> photos.id)", builder.ToSql());
        }

        [Fact]
        public void TestComplexJoins()
        {
            var builder = GetBuilder();
            var a = System.DateTime.Now;
            builder.From("users").InnerJoin("contacts", "users.id", "contacts.id", (Builder b) =>
            {
                b.Where("users2.id", "=", Builder.Raw("contacts.id"));
            }).LeftJoin("contacts", "users.id", "contacts.id", (Builder b) =>
            {
                b.Where("users2.id", "=", Builder.Raw("contacts.id")).WhereBetween("id", Builder.Raw("'abc'"), a);
            });

            Assert.Equal("SELECT * FROM users INNER JOIN contacts ON (users.id = contacts.id) AND " +
                "(users2.id = contacts.id) LEFT JOIN contacts ON (users.id = contacts.id) AND (users2.id = contacts.id AND id BETWEEN 'abc' AND @0)", builder.ToSql());

            Assert.Equal(builder.Parameters["@0"], a);
        }


        [Fact]
        public void TestSQLiteRightJoins()
        {
            var builder = GetSQLiteBuilder();
            builder.From("users").InnerJoin("contacts", "users.id", "contacts.id")
                .LeftJoin("photos", "users.id", "photos.id")
                .RightJoin("photos", "users.id", "photos.id");
            Assert.Equal("SELECT * FROM users INNER JOIN contacts ON (users.id = contacts.id) " +
                "LEFT JOIN photos ON (users.id = photos.id) LEFT JOIN photos ON (photos.id = users.id)", builder.ToSql());

        }

        [Fact]
        public void TestTakeAndSkip()
        {
            var builder = GetMySqlBuilder();
            builder.Select("*").From("users").Take(10);
            Assert.Equal("SELECT * FROM users LIMIT 10", builder.ToSql());

            builder = GetMySqlBuilder();
            builder.Select("*").From("users").Skip(10);
            Assert.Equal("SELECT * FROM users LIMIT 0 OFFSET 10", builder.ToSql());

            builder = GetMySqlBuilder();
            builder.Select("*").From("users").Skip(10).Take(10);
            Assert.Equal("SELECT * FROM users LIMIT 10 OFFSET 10", builder.ToSql());

            builder = GetMySqlBuilder();
            builder.Select("*").From("users").Skip(10).Take(10).OrderByDesc("Email");
            Assert.Equal("SELECT * FROM users ORDER BY Email DESC LIMIT 10 OFFSET 10", builder.ToSql());

            builder = GetMySqlBuilder();
            builder.Select("*").From("users").Skip(10).Take(10).OrderByDesc("Email").Where("id", 1);
            Assert.Equal("SELECT * FROM users WHERE id = @0 ORDER BY Email DESC LIMIT 10 OFFSET 10", builder.ToSql());
        }

        [Fact]
        public void TestSqlServerTakeAndSkip()
        {
            var builder = GetSqlServerBuilder();
            builder.Select("*").From("users").Take(10);
            Assert.Equal("SELECT TOP 10 * FROM users", builder.ToSql());

            builder = GetSqlServerBuilder();
            builder.Select("*").From("users").Skip(10);
            Assert.Equal("SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS ROW_NUM FROM users) AS TEMP_TABLE WHERE ROW_NUM >= 11", builder.ToSql());

            builder = GetSqlServerBuilder();
            builder.Select("*").From("users").Skip(10).Take(10);
            Assert.Equal("SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS ROW_NUM FROM users) AS TEMP_TABLE WHERE ROW_NUM BETWEEN 11 AND 20", builder.ToSql());

            builder = GetSqlServerBuilder();
            builder.Select("*").From("users").Skip(10).Take(10).OrderByDesc("Email").Where("id", 1);
            Assert.Equal("SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY Email DESC) AS ROW_NUM FROM users WHERE id = @0) AS TEMP_TABLE WHERE ROW_NUM BETWEEN 11 AND 20", builder.ToSql());
        }

        [Fact]
        public void TestBasicWrap()
        {
            var builder = GetBuilder();

            Assert.Equal("\"a\" AS \"b\"", builder.WrapValue("a as b"));
            Assert.Equal("\"a\" AS \"b\"", builder.WrapValue("  a   as    b  "));
            Assert.Equal("\"test\"", builder.WrapValue("test"));
            Assert.Equal("\"test\"", builder.WrapValue("test  "));
            Assert.Equal("\"test\"", builder.WrapValue("  test  "));
            Assert.Equal("\"test\"", builder.WrapValue("  test"));
            Assert.Equal("\"a\".\"b\".\"c\" AS \"d\"", builder.WrapValue("a.b.c d"));
            Assert.Equal("\"a\".\"b\".\"c\" AS \"d\"", builder.WrapValue("a.b.c    d"));
            Assert.Equal("\"a\" AS \"d\"", builder.WrapValue(" a    d"));
        }

        [Fact]
        public void TestMySqlWrap()
        {
            var builder = GetMySqlBuilder();

            Assert.Equal("`a` AS `b`", builder.WrapValue("a as b"));
            Assert.Equal("`a` AS `b`", builder.WrapValue("  a   as    b  "));
            Assert.Equal("`test`", builder.WrapValue("test"));
            Assert.Equal("`test`", builder.WrapValue("test  "));
            Assert.Equal("`test`", builder.WrapValue("  test  "));
            Assert.Equal("```test```", builder.WrapValue("  `test`"));
            Assert.Equal("`a`.`b`.`c` AS `d`", builder.WrapValue("a.b.c d"));
            Assert.Equal("`a`.`b`.`c` AS `d`", builder.WrapValue("a.b.c    d"));
            Assert.Equal("`a` AS `d`", builder.WrapValue(" a    d"));
        }

        [Fact]
        public void TestSqlServerWrap()
        {
            var builder = GetSqlServerBuilder();

            Assert.Equal("[a] AS [b]", builder.WrapValue("a as b"));
            Assert.Equal("[a] AS [b]", builder.WrapValue("  a   as    b  "));
            Assert.Equal("[test]", builder.WrapValue("test"));
            Assert.Equal("[test]", builder.WrapValue("test  "));
            Assert.Equal("[test]", builder.WrapValue("  test  "));
            Assert.Equal("[test]", builder.WrapValue("  test"));
            Assert.Equal("[[test]]]", builder.WrapValue("  [test]"));
            Assert.Equal("[a].[b].[c] AS [d]", builder.WrapValue("a.b.c d"));
            Assert.Equal("[a].[b].[c] AS [d]", builder.WrapValue("a.b.c    d"));
            Assert.Equal("[a] AS [d]", builder.WrapValue(" a    d"));
        }

        [Fact]
        public void TestNewBuilder()
        {
            var builder = GetMySqlBuilder();
            Assert.Equal(builder.SqlSyntax.GetType().ToString(), builder.NewBuilder().SqlSyntax.GetType().ToString());

            builder = GetSqlServerBuilder();
            Assert.Equal(builder.SqlSyntax.GetType().ToString(), builder.NewBuilder().SqlSyntax.GetType().ToString());

            builder = GetPostgresBuilder();
            Assert.Equal(builder.SqlSyntax.GetType().ToString(), builder.NewBuilder().SqlSyntax.GetType().ToString());

            builder = GetBuilder();
            Assert.Equal(builder.SqlSyntax.GetType().ToString(), builder.NewBuilder().SqlSyntax.GetType().ToString());
        }

        public Builder GetBuilder()
        {
            return new Builder();
        }

        public Builder GetMySqlBuilder()
        {
            return new Builder(new MySqlSyntax());
        }

        public Builder GetSQLiteBuilder()
        {
            return new Builder(new SQLiteSyntax());
        }

        public Builder GetSqlServerBuilder()
        {
            return new Builder(new SqlServerSyntax());
        }

        public Builder GetPostgresBuilder()
        {
            return new Builder(new PostgresSyntax());
        }
        #endregion
    }
}
