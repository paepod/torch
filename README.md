## Introduction
Torch database supports connection database system via ADO.Net providers. Currently, It have been tested two database systems:

* **SQLite**
* **Microsoft SQL Server**

`Torch` is the project name and but everything will start with `Lantern` class which it is `facade`.

#### Configuration
Torch database makes connecting with databases and running queries extremely simple. The database configuration for your application by using `Lantern.AddConfig` method on the `Lantern` facade.
The AddConfig require parameters as follows  **The first configuration will be the default configuration.** 

    Lantern.AddDBConfig(string configName, string xConnectionString, DbProvider xDbProvider, DbSyntax xDbSyntax, bool isDefault = false)
    Lantern.AddDBConfig(DbConfig dbConfig)

For some reasons you may want to change the default configuration, You can do it by using `Lantern.SetDefaultDBConfig` method.

    Lantern.SetDefaultDBConfig(string configName)

Once you have configured your database connection, you may run queries using the Lantern facade on `DB` method. The `DB` method requires 2 arguments. The first arguement is table name. The configuration name is the second argument that you may pass or not pass.if you don't pass the configuration name. Lantern will use the default configuration instead. 

	Lantern.DB("customer").Get();
	
#### Using Mutiple Database Connection

When using multiple connections, you may access each connection via the `DbConnection` method or `DB` on the `Lantern` facade. 
The name passed to the connection method should correspond to one of the connections listed in your configuration:

    Builder DBConnection(string configName = "")
	Builder DB(string table = null, string configName = "")

Here is the examples:

	Lantern.DbConnection("sqlserver").Table("customer").Get();
	Lantern.DB("customer","sqlserver").Get();
	Lantern.DB("customer","sqlite").Get();
	
#### A General Statement

Some database operation should not return any value such as `DROP` `ALTER`, `CREATE TABLE`. For these reasons, you might use the `DbExecuteStatement` method on the `Lantern` facade:
	
	int DBExecuteStatement(string sql, params object[] parameters)

	Lantern.DbExecuteStatement("IF OBJECT_ID('Rainbows', 'U') IS NOT NULL DROP TABLE Rainbows;");
	
You can also pass the statement parameters as follows:

	Lantern.DBExecuteStatement("IF OBJECT_ID('@Visitors', 'U') IS NOT NULL DROP TABLE Visitors;", new { Visitors = "Visitors" });

Perhap you use another connections instead of default connection, you can use `DBConnection` method followed by `ExecuteStatement` method on the query builder instance:

	Lantern.DBConnection("configname").ExecuteStatement("IF OBJECT_ID(@Friends, 'U') IS NOT NULL DROP TABLE Friends;", new { Friends = "Friends" });
	
## Query Builder
 The SQL builder provides a fluent method to create and running database queries. It can be used to perform most database operation. Troch builder uses ADO.Net provider to make connecting and executing queries and non-queries. it also used SQL parameters for preventing SQL Injection.

#### Retrieving All records (`DataSet`) From A Table
To begin a query builder, use the `DB` method on the `Lantern` facade. The `DB` method returns a query builder instance for the given table. the following is the example to `Get` All records (`DataSet` or `IEnumerable<T>`) from a table:

    DataSet ds = Lantern.DB("customer").Get();	
	IEnumerable<dynamic> customers = Lantern.DB("customer").Get<dynamic>();
	var customer = customers.Single();
	
The example above will produce the following SQL:

    SELECT * FROM customers;

#### From Subquery
Sometime you may want to use the table identified in the From clause as a a subquery. Please have a look the following examples where a subquery is used in the FROM clause of a SELECT statement:


** Builder Instance **
Lantern `DB` method allows you to pass `Builder` instance. You may identify table alias name or not.

	DB(Builder builder, string configName = "")
	DB(string aliasName, Builder builder, string configName = "")

	Lantern.DB(builder).ToSql();
	Lantern.DB("builder",builder).ToSql();
	
the above examples will produce the queries below:
	
	SELECT * FROM (SELECT * FROM TEST)
	SELECT * FROM (SELECT * FROM TEST) AS builder

** Anonymous Function **	
Lantern `DB` method allows you to pass `Anonymous function` which its first argument must be a Builder instance. You may identify table alias name or not.

	DB(Action<Builder> closure, string configName = "")
	DB(string aliasName, Action<Builder> closure, string configName = "")

	Action<Builder> closure = (Builder b) => { 
		b.Select("*").From("TEST"); 
	};
	
	Lantern.DB(closure).ToSql();
	Lantern.DB("closure",builder).ToSql();
	
the above examples will produce the queries below:

	SELECT * FROM (SELECT * FROM TEST)
	SELECT * FROM (SELECT * FROM TEST) AS closure
	
#### Retrieving a value From A Query
Use the `GetValue` method to retrieve a single value (for example, an aggregate value) from a database.

    var maxid = Lantern.DB("customer").Select("MAX(id)").GetValue();
	
## Selects

#### Specifying A Select Clause	
You may not always select all columns from a database table. You can specify list of column name for the query by using `Select` method:

	DataSet ds = Lantern.DB("customer").Select("FirstName", "Lastname", "Email").Select("Pasword").Get()

The above examples will produce the sql below:

    SELECT FirstName, Lastname, Email, Pasword FROM customer
	
Sometime You want to pass subquery instead of column name. You can do it by passing the alias name of subquery and `Anonymous function` to generate subquery.
 
	DataSet ds = Lantern.DB("customer")
					.Select("FirstName")
					.Select("LastName")
					.Select("AliasName", 
					(Builder b) => { 
						b.Select("MAX(id)").From("B");
					}).Get();

The above examples will produce the sql below:
    
	SELECT FirstName, LastName, (SELECT MAX(id) FROM B) AS AliasName FROM customers

	
#### Raw Value 
 You may need to use raw text in a query. These text will be injected into the sql query as string, so be careful to use the `Lantern.DBRaw(object value)` method.

    Lantern.DB("customers").Select("FirstName").Where("id", "=", Lantern.DBRaw(1)).Where("Firstname", "=", "Austin").ToSql();
  
The example above will produce the following SQL:

    SELECT FirstName FROM customers WHERE id = 1 AND Firstname = @0"
	
##Unions / Unions All
	
We provides a quick way to "union" two or more queries together. 

For example, you must create a first query, and then use the `Union` method to `Union` it with a second query:

    Builder builder1 = Lantern.DB("customers").Where("id", "=", "1")
    Builder builder2 = Lantern.DB("customers").Where("id", "=", "2")
    DataSet ds =  = Lantern.DB("customers").Where("id", "=", "3").Union(builder2).Union(builder1);     

The example above will produce the SQL as follows:

	SELECT * FROM customers WHERE id = @0 UNION SELECT * FROM customers WHERE id = @000_1 UNION SELECT * FROM customers WHERE id = @000_2

The `UnionAll` method is also available and has the same method signature as union.	

## Join

#### Basic Joins

The query builder provide you to build write join statements. To perform a `Inner join , Left join and Right join`, you may use the `InnerJoin`, `LeftJoin`, `RightJoin` method on a query builder instance.  The must pass table name followed by the column constraints for the join.
	
	InnerJoin(string xTable, string xColumn1, string xColumn2, Action<Builder> closure = null, string xOperator = "=", string xBoolean = "AND")
    LeftJoin(string xTable, string xColumn1, string xColumn2, Action<Builder> closure = null, string xOperator = "=", string xBoolean = "AND")
	RightJoin(string xTable, string xColumn1, string xColumn2, Action<Builder> closure = null, string xOperator = "=", string xBoolean = "AND")
    
    DataSet ds = Lantern.DB("users").InnerJoin("contacts", "users.id", "contacts.id")
                .LeftJoin("photos", "users.id", "photos.id")
                .RightJoin("photos", "users.id", "photos.id", xOperator: "<>").Get();

As you can see, you can join to multiple tables in a single query and can use [Named Arguments](https://msdn.microsoft.com/en-us/library/dd264739.aspx) to specify another the operator instead of the equal (`=`).  The example above will produce the SQL as follows:	
        
	SELECT * FROM users 
	INNER JOIN contacts ON (users.id = contacts.id) 
	LEFT JOIN photos ON (users.id = photos.id) 
	RIGHT JOIN photos ON (users.id <> photos.id)

#### Complex Joins
Sometime you might want to use where style clause on join statements. you can use the `Where` and `OrWhere` method on a join by creating `Anonymous function` as the forth argument. The following is the example of join where style clauses.

	DataSet ds = Lantern.DB("users")
	.InnerJoin("contacts", "users.id", "contacts.id", (Builder b) => {
                b.Where("users2.id", "=", Lantern.DBRaw("contacts.id"));
            })
	.LeftJoin("contacts", "users.id", "contacts.id", (Builder b) => {
                b.Where("users2.id", "=", Lantern.DBRaw("contacts.id")).WhereBetween("id", Lantern.DBRaw("'abc'"), a);
            }).Get();
			
The example above will produce the SQL as follows:	
		
	SELECT * FROM users 
	INNER JOIN contacts ON (users.id = contacts.id) AND (users2.id = contacts.id) 
	LEFT JOIN contacts ON (users.id = contacts.id) AND (users2.id = contacts.id AND id BETWEEN 'abc' AND @0)


	
## Where Clauses

#### Basic Where Clauses

You may want to use `Where` Method on a query builder instance to specify the search condition for the rows returned by the query. The method requires simple three arguments. 
The first argument is the column name, the second is an operator, such as `=`, `<>`, `IS`, `IS NOT`, and `LIKE`. The last argument is a filter value.

For example, this is a query that verifies customer of the "id" column is equal to 3:

    DataSet ds = Lantern.DB("customer").Where("id","=",3).Get();
	
However, if the operator is the equal (`=`) , you can pass the filter value directly as the second to the `Where` method:
	
    DataSet ds = Lantern.DB("customer").Where("id",3).Get();
	
Both above examples will produce the sql below:

    SELECT * FROM customer WHERE id = @0

You may want to use other operators when writing a `Where` clause:

    DataSet ds = Lantern.DB("customer").Where("id","<>",3).Get();
    DataSet ds = Lantern.DB("customer").Where("Name","LIKE","%Shinnawat%).Get();
	DataSet ds = Lantern.DB("customer").Where("updated_date", "is", null).Get();
	DataSet ds = Lantern.DB("customer").Where("updated_date", "is not", null).Get();
						
These examples will produce the sql queries below:

    SELECT * FROM customer WHERE id <> @0
    SELECT * FROM customer WHERE Name LIKE @0
    SELECT * FROM customers WHERE updated_date IS NULL
    SELECT * FROM customers WHERE updated_date IS NOT NULL

#### And / Or Statements

You may add more where `AND` clause as well as where `OR` clause to the query. 

For where `AND` clause, this is simply to use `Where*` method:
	
	Lantern.DB("Customers").Select("FirstName").Where("id", "=", 1).Where("FirstName", "=", "Austin").ToSql();
	
The query builder will produce the sql below:
	
	SELECT * FROM customer WHERE id = @0 AND FirstName = @1

For where `OR` clause, The `OrWhere*` method is required. This method accpets the same argument as the `Where` method:

	Lantern.DB("Customers").Select("FirstName").Where("id", "=", 1).OrWhere("Firstname", "=", "Austin").ToSql();
	
#### Advance Where Clauses
**WhereBetween / WhereNotBetween / OrWhereBetween / OrWhereNotBetween**

Specifies a range to test.

	Lantern.DB("Customers").Select("id").WhereBetween("id", 1, 2).ToSql();
	Lantern.DB("Customers").Select("id").WhereNotBetween("id", 1, 2);
	Lantern.DB("Customers").Select("id").Where("id", "<", 3).OrWhereBetween("id", 1, 2).ToSql();
	Lantern.DB("Customers").Select("id").Where("id", "<", 3).OrWhereNotBetween("id", 1, 2).ToSql();
	
The first example will produce the sql below:

	SELECT id FROM customers WHERE id BETWEEN @0 AND @1

The thrid example will produce the sql below:

	SELECT id FROM customers WHERE id < @0 OR id BETWEEN @1 AND @2

** WhereIn / WhereNotIn /  OrWhereIn / OrWhereNotIn **

Determines whether a specified value matches any value in a subquery or a list.

List examples:

	Lantern.DB("Customers").Select("id").WhereIn("id", 1, 2, 3, 4, 5).ToSql();
	Lantern.DB("Customers").Select("id").WhereNotIn("id", 1, 2, 3, 4, 5).ToSql();
	Lantern.DB("Customers").Select("id").Where("id", "<", 6).OrWhereIn("id", 1, 2, 3, 4, 5).ToSql();
	Lantern.DB("Customers").Select("id").Where("id", "<", 6).OrWhereNotIn("id", 1, 2, 3, 4, 5).ToSql();

The first example will produce the sql below:

	SELECT FirstName FROM customers WHERE id IN (@0,@1,@2,@3,@4,@5) 

The thrid example will produce the sql below:

	SELECT FirstName FROM customers WHERE id < @0 AND id IN (@1,@2,@3,@4,@5,@6) 		
	
Subquery examples:

	Lantern.DB("Customers")
		.Select("id")
		.WhereIn("id", (Builder b) => { 
							b.From("test").Where("id", "<", 10); 
						})
		.Where("id", "<", 3).ToSql();

As you see, passing the `Anonymous function` to instructs the query builder to produce the subquery.

    SELECT FirstName FROM customers WHERE id IN (SELECT * FROM test WHERE id < @0) AND id < @1

**WhereExits / WhereNotExits / OrWhereExits / OrWhereNotExits **

Specifies a subquery to test for the existence of rows:

	Lantern.DB("Customers")
		.Select("id")
		.Where("id", "<", 3)
		.WhereExits((Builder b) => { 
			b.From("test").Where("id", "<", 10); 
		}).ToSql();

	SELECT id FROM customers WHERE id < @0 AND EXITS (SELECT * FROM test WHERE id < @1)

#### Where Clauses Grouping

	Lantern.DB("Customers")
	.Select("Firstname", "Lastname")
	.From("customers")
	.Where("Email", "=", "1234")
	.Where("id", "=", (Builder b) => {
		b.Select("max(id)").From("customers").Where("Email", "=", "testmail");
	})
	.Where("Firstname", "LIKE", "%PAE%");

As you can see, passing `Closure` into the `OrWhere` method instructs the query builder to begin a constraint group.
	
The example above will produce the following SQL:
    
	SELECT Firstname, Lastname FROM customers WHERE Email = @0 AND (id = (SELECT max(id) FROM customers WHERE Email = @1)) AND Firstname LIKE @2
	
## GroupBy / OrderBy / Having

#### GroupBy And Having

The `GroupBy` method groups the query resutls and `Having` method is similar to the `Where` method but it specifies a search condition for a group or an aggregate.

	GroupBy(params string[] xGroup)
	
    Lantern.DB("customers").Select("Count(*)").GroupBy("id", "Email").ToSql();
    
Below is the query that generates from the above example:
	
	SELECT Count(*() FROM customers GROUP BY id, Email

Let's shows you the set of `Having methods`. Actually the `Having` clause is similar to `Where` clause but you must pass at lease 3 arguments. 

	Having(string xColumn, string xOperator, object xValue)
	OrHaving(string xColumn, string xOperator, object xValue)
	
Having clauses is allowed you to pass subquery instead of value:
	Having(string xColumn, string xOperator, Action<Builder> closure)
	OrHaving(string xColumn, string xOperator, Action<Builder> closure)

We will show you the complex having  example:

	Lantern.DB("customers")
        .Where("Email", "=", "1234")
		.GroupBy("id", "Email")
        .Having("SUM(Salary)", ">", 500)
        .Having("SUM(Salary)", "=", (Builder b) => {
				b.Select("SUM(INCOME)").From("customers").Where("Email", "=", "testmail");
			})
        .OrHaving("COUNT(*)", ">", 1000).ToSql().

The above example will produce the sql below:

	SELECT * FROM customers WHERE Email = @0 GROUP BY id, Email HAVING SUM(Salary) > @1 AND (SUM(Salary) = (SELECT SUM(INCOME) FROM customers WHERE Email = @2)) OR COUNT(*) > @3
	
#### OrderBy 
The `OrderBy` method is allowed to sort data returned by a query. You may specify that the values in the specified column should be sorted in ascending (`OrderByASC`) or descending (`OrderByDesc`) order. 

`Lantern` provides 3 methods for OrderBy clauses. The first method is OrderBy that you may pass 1 or 2 argument(s). The frist argument is the column name and the second is the direction. The default direction is `Ascending`
	
	OrderBy(string xColumn, string xDirection = "ASC")
	
For convenient, Lantern also provides you `OrderByAsc` and OrderByDesc` which you can multiple columns.

	OrderByAsc(params string[] xColumn)
	OrderByDesc(params string[] xColumn)

The following is the example of `OrderBy clauses`:
	
	Lantern.DB("Customers").OrderBy("Email").OrderByDesc("Age", "Phone").OrderByAsc("Data1", "Data2").toSql();

This above example is generate to sql query as follows:

	SELECT * FROM customers ORDER BY Email ASC, Age DESC, Phone DESC, Data1 ASC, Data2 ASC
	
##  Offset Fetch / Skip Take
To provides you with an option to limit the number of results returned from the query. you may use the skip and take methods:

	Troch.DB("customers").Select("*").Take(10).Get();
	Troch.DB("customers").Select("*").Skip(10).Get();	
	Troch.DB("customers").Select("*").Take(10).Skip(10).Get()

The above example set will produce:	
for Microsoft Sql Server 2005 onward

	SELECT TOP 10 * FROM customers
	SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS ROW_NUM FROM customers) AS TEMP_TABLE WHERE ROW_NUM >= 11
	SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS ROW_NUM FROM customers) AS TEMP_TABLE WHERE ROW_NUM BETWEEN 11 AND 20
	SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY Email DESC) AS ROW_NUM FROM customers) AS TEMP_TABLE WHERE ROW_NUM BETWEEN 11 AND 20
        SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS ROW_NUM FROM customers) AS TEMP_TABLE WHERE ROW_NUM >= 11 

for SQLite
	SELECT * FROM customers LIMIT 10
	SELECT * FROM customers LIMIT 0 OFFSET 10
	SELECT * FROM customers LIMIT 10 OFFSET 10
	SELECT * FROM customers ORDER BY Email DESC LIMIT 10 OFFSET 10
	



## Insert / Update / Delete

The `Insert`, `Update` and `Delete` mehtod will returns the number of rows affected

#### Insert
Of course, You may need to insert a new records into table. The `Insert` method accepts an `Array` of `Anonymous Object` which contain column names and its values to insert. 
	
	int Insert(params object[] xValues)
	var result = Lantern.DB("test").Insert(new { Amount = 109, Messages = "4" }, new { Amount = 108, Messages = "2" })

#### Update
You may update exitsing records in the database. The update method, returns the number of rows affected, accepts `Anonymous Object` which are value pairs containing the columns to be updated:
You may constrain the update query using where clauses:
	
	int Update(object xValues)
	var result = Lantern.DB("test").Where("id", "=", 2).Update(new { Amount = 9999, Messages = "3" })

** You must call `Where` method before `Update` method because the `Update` method will trigger update query suddenly when you used it. **	
	
#### Delete
You may want to delete your data in the table. You can use `Delete` method which you can constrain the delete query using where clauses:

	int result = Lantern.DB("test").Where("id", "=", 1).Delete();
  
** You must call `Where` method before `Delete` method because the `Delete` method will trigger delete query suddenly when you used it. **	


Sometime your table might specify column name `id` as a primary key, Therefore we provide you a convenient method `DeleteById`. The `DeleteById` method is required you to pass 2 arguments, the first one is number of id. The second is the column name for evaluating against the query. the default column name is `id`

    DeleteById(object id = null, string column = "id")
    result = Lantern.DB("test").DeleteById(2)